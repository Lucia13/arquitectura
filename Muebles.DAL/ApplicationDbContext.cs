﻿using Muebles.CORE;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;


namespace Muebles.DAL
{

    // Enable-Migrations
    // Add-Migration "Start"
    // Update-Database -StartUpProjectName "Muebles.Web" -ConnectionStringName "DefaultConnection" -Verbose

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// Colección de productos persistibles
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Colección de pedidos persistibles
        /// </summary>
        public DbSet<Order> Orders { get; set; }


        /// <summary>
        /// Colección de productos en los pedidos
        /// </summary>
        public DbSet<OrderProduct> OrderProducts { get; set; }

        /// <summary>
        /// Colección de imágenes
        /// </summary>
        public DbSet<Picture> Pictures { get; set; }


        /// <summary>
        /// Colección de direcciones
        /// </summary>
        public DbSet<Address> Addresses { get; set; }
    }
}
