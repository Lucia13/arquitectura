﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Muebles.MVC.Controllers;

namespace Muebles.MVC.Tests.Controllers
{

    ///<summary>
    /// Prueba de los métodos de la clase OrderProductController
    /// </summary>
    [TestClass]
    public class OrderProductControllerTest
    {

        ///<summary> 
        /// Prueba de si el método Index() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void IndexOrderProduct()
        {
            // Arrange
            OrderProductController controller = new OrderProductController();

            // Act
            ViewResult result = controller.Index(1036) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        ///<summary> 
        /// Prueba de si el método Create() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void CreateOrderProduct()
        {
            // Arrange
            OrderProductController controller = new OrderProductController();

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


        ///<summary> 
        /// Prueba de si el método Edit() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void EditOrderProduct()
        {
            // Arrange
            OrderProductController controller = new OrderProductController();

            // Act
            ViewResult result = controller.Edit(2094) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        ///<summary> 
        /// Prueba de si el método Delete() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void DeleteOrderProduct()
        {
            // Arrange
            OrderProductController controller = new OrderProductController();

            // Act
            ViewResult result = controller.DeleteItem(2097) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

      



    }
}
