﻿
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Muebles.MVC.Controllers;
using Muebles.MVC.Controllers.Admin;

namespace Muebles.MVC.Tests.Controllers
{
    ///<summary>
    /// Prueba de los métodos de la clase HelpController
    /// </summary>
    [TestClass]
    public class HelpControllerTest
    {
        ///<summary> 
        /// Prueba de si el método HelpUser() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void HelpUser()
        {
            // Arrange
            HelpController controller = new HelpController();

            // Act
            ViewResult result = controller.HelpUser() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


        ///<summary> 
        /// Prueba de si el método HelpAdmin() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void HelpAdmin()
        {
            // Arrange
            HelpController controller = new HelpController();

            // Act
            ViewResult result = controller.HelpAdmin() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
