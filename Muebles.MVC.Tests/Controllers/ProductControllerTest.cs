﻿
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Muebles.MVC.Controllers;


namespace Muebles.MVC.Tests.Controllers
{

    ///<summary>
    /// Prueba de los métodos de la clase ProductController
    /// </summary>
    [TestClass]
    public class ProductControllerTest
    {

        ///<summary> 
        /// Prueba de si el método Index() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void IndexProduct()
        {
            // Arrange
            ProductController controller = new ProductController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        ///<summary> 
        /// Prueba de si el método Edit() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void EditProduct()
        {
            // Arrange
            ProductController controller = new ProductController();

            // Act
            ViewResult result = controller.Edit(1017) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


        ///<summary> 
        /// Prueba de si el método Details() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void DetailsProduct()
        {
            // Arrange
            ProductController controller = new ProductController();

            // Act
            ViewResult result = controller.Details(1017) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
