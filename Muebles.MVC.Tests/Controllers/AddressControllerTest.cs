﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Muebles.MVC.Controllers;
using System.Web.Mvc;

namespace Muebles.MVC.Tests.Controllers
{
    ///<summary>
    /// Prueba de los métodos de la clase AddressController
    /// </summary>
    [TestClass]
    public class AddressControllerTest
    {
        
        ///<summary> 
        /// Prueba de si el método Edit() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void EditAddress()
        {
            // Arrange
            AddressController controller = new AddressController();

            // Act
            ViewResult result = controller.Edit() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
