﻿using Muebles.MVC.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace Muebles.MVC.Tests.Controllers
{

    ///<summary>
    /// Prueba de los métodos de la clase ResumeController
    /// </summary>
    [TestClass]
    public class ResumeControllerTest
    {

        ///<summary> 
        /// Prueba de si el método Index() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void IndexResume()
        {
            // Arrange
            ResumeController controller = new ResumeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
