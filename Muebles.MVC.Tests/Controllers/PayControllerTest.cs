﻿using System;
using Muebles.MVC.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace Muebles.MVC.Tests.Controllers
{

    ///<summary>
    /// Prueba de los métodos de la clase PayController
    /// </summary>
    [TestClass]
    public class PayControllerTest
    {

        ///<summary> 
        /// Prueba de si el método Create() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void CreatePay()
        {
            // Arrange
            PayController controller = new PayController();

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
