﻿using System;
using Muebles.MVC.Controllers;
using Muebles.MVC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Muebles.MVC.Tests.Controllers
{

    ///<summary>
    /// Prueba de los métodos de la clase OrderController
    /// </summary>
    [TestClass]
    public class OrderControllerTest
    {

        ///<summary> 
        /// Prueba de si el método Index() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void IndexOrder()
        {
            // Arrange
            OrderController controller = new OrderController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        ///<summary> 
        /// Prueba de si el método Edit() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void EditOrder()
        {
            // Arrange
            OrderController controller = new OrderController();

            // Act
            ViewResult result = controller.Edit(1037) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


        ///<summary> 
        /// Prueba de si el método Details() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void DetailsOrder()
        {
            // Arrange
            OrderController controller = new OrderController();

            // Act
            ViewResult result = controller.Details(1036) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

   

    }
}
