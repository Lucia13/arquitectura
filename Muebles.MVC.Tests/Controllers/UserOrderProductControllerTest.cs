﻿
using Muebles.MVC.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace Muebles.MVC.Tests.Controllers
{

    ///<summary>
    /// Prueba de los métodos de la clase UserOrderProductController
    /// </summary>
    [TestClass]
    public class UserOrderProductControllerTest
    {

        ///<summary> 
        /// Prueba de si el método Index() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void IndexUserOrderProduct()
        {
            // Arrange
            UserOrderProductController controller = new UserOrderProductController();

            // Act
            ViewResult result = controller.Index(2094) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

      

    }
}
