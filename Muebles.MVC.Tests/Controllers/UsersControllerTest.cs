﻿using System;
using Muebles.MVC.Controllers;
using Muebles.MVC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Muebles.MVC.Tests.Controllers
{

    ///<summary>
    /// Prueba de los métodos de la clase UsersController
    /// </summary>
    [TestClass]
    public class UsersControllerTest
    {

        ///<summary> 
        /// Prueba de si el método Edit() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void EditUsers()
        {
            // Arrange
            UsersController controller = new UsersController();

            // Act
            ViewResult result = controller.Edit("39b74677 - f1e2 - 45e4 - b93d - 85fd7098bc05") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


        ///<summary> 
        /// Prueba de si el método Details() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void DetailsUsers()
        {
            // Arrange
            UsersController controller = new UsersController();

            // Act
            ViewResult result = controller.Details("39b74677 - f1e2 - 45e4 - b93d - 85fd7098bc05") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        } 

    }
}
