﻿using System;
using Muebles.MVC.Controllers;
using Muebles.MVC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Muebles.MVC.Tests.Controllers
{

    ///<summary>
    /// Prueba de los métodos de la clase CartController
    /// </summary>
    [TestClass]
    public class CartControllerTest
    {

        ///<summary> 
        /// Prueba de si el método Index() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void IndexCart()
        {
            // Arrange
            CartController controller = new CartController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        ///<summary> 
        /// Prueba de si el método EditSumarRestar() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void EditSumarCart()
        {
            // Arrange
            CartController controller = new CartController();

            // Act
            ViewResult result = controller.EditSumarRestar(2063, true) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


        ///<summary> 
        /// Prueba de si el método EditSumarRestar() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void EditRestarCart()
        {
            // Arrange
            CartController controller = new CartController();

            // Act
            ViewResult result = controller.EditSumarRestar(2063, false) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


        ///<summary> 
        /// Prueba de si el método Delete() no devuelve una vista nula
        /// </summary>
        [TestMethod]
        public void DeleteItemCart()
        {
            // Arrange
            CartController controller = new CartController();

            // Act
            ViewResult result = controller.DeleteItem(2067) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

      

    }
}
