﻿using Muebles.CORE;
using System.Linq;


namespace Muebles.Application
{
    /// <summary>
    /// Interfaz de la clase ProductManager
    /// </summary>
    public interface IProductManager
    {
        Product GetById(int id);

        Product RemoveById(int id);

        IQueryable<Product> GetAllWithStock();

        IQueryable<Product> GetAll();

        IQueryable<Product> GetAllByCategory(string categoria);

        Product Add(Product product);

        string ToStringCategory(Category category);
    }
}

