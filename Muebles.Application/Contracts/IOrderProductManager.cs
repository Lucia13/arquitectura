﻿using Muebles.CORE;
using System.Linq;

namespace Muebles.Application
{

    /// <summary>
    /// Interfaz de la clase OrderProductManager
    /// </summary>
    public interface IOrderProductManager
    {

        IQueryable<OrderProduct> GetOPByOrderId(int orderId);

        OrderProduct GetById(int id);

        OrderProduct RemoveById(int id);

        OrderProduct Add(OrderProduct orderProduct);
    }
}

