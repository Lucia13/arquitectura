﻿using Muebles.CORE;
using System.Linq;

namespace Muebles.Application
{
    /// <summary>
    /// Interfaz de la clase OrderManager
    /// </summary>
    public interface IOrderManager
    {
        Order GetById(int id);

        IQueryable<Order> GetAllByUserId(string userId);

        Order GetByUserIdTodayPending(string userId);

        Order GetByUserIdTodayPayed(string userId);

        Order RemoveById(int id);

        IQueryable<Order> GetAll();

        Order Add(Order order);
    }
}
