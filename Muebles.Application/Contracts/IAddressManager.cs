﻿using Muebles.CORE;
using System;


namespace Muebles.Application
{
    /// <summary>
    /// Interfaz de la clase AddressManager
    /// </summary>
    public interface IAddressManager
    {
        Address GetByUser(string UserId);
    }
}
