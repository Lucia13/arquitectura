﻿using Muebles.CORE;
using System.Linq;

namespace Muebles.Application
{
    /// <summary>
    /// Interfaz de la clase PictureManager
    /// </summary>
    public interface IPictureManager
    {
        IQueryable<Picture> GetAllByProduct(int productId);
        Picture RemoveByRoute(string ruta);
        string GetRoute(int productId, int numero);

        Picture RemoveByProductId(int id, int number);

        Picture Add(Picture picture);
    }
}
