﻿
using Muebles.CORE;
using Muebles.DAL;
using System.Collections.Generic;
using System.Linq;

namespace Muebles.Application
{
    /// <summary>
    /// Clase para manejar imágenes
    /// </summary>
    public class AddressManager : Manager<Address>, IAddressManager
    {
        /// <summary>
        /// Constructor del manager de imágenes
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public AddressManager(ApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo que retorna todas los imágenes de un producto
        /// </summary>
        /// <param name="productId">Id del producto</param>
        /// <returns> IQueryable de imágenes </returns>
        public Address GetByUser(string User_Id)
        {
            return Context.Addresses.Where(i => i.User_Id == User_Id).SingleOrDefault(); ;
        }

       
    }
}
