﻿
using Muebles.CORE;
using Muebles.DAL;
using System.Collections.Generic;
using System.Linq;

namespace Muebles.Application
{
    /// <summary>
    /// Clase para manejar imágenes
    /// </summary>
    public class PictureManager : Manager<Picture>, IPictureManager
    {
        /// <summary>
        /// Constructor del manager de imágenes
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public PictureManager(ApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo que retorna todas los imágenes de un producto
        /// </summary>
        /// <param name="productId">Id del producto</param>
        /// <returns> IQueryable de imágenes </returns>
        public IQueryable<Picture> GetAllByProduct(int productId)
        {
            return Context.Pictures.Where(i => i.ProductId == productId);
        }

        /// <summary>
        /// Metodo que elimina la imagen con la ruta pasada por parámetro
        /// </summary>
        /// <param name="ruta">Ruta de la imagen</param>
        /// <returns>Imagen eliminada</returns>
        public Picture RemoveByRoute(string ruta)
        {
            return Context.Pictures.Remove(Context.Pictures.Where(i => i.Route == ruta).SingleOrDefault());
        }

        /// <summary>
        /// Metodo que elimina la imagen del producto con el id pasado por parámetro
        /// </summary>
        /// <param name="id">Id del producto</param>
        /// <returns>Imagen eliminada</returns>
        public Picture RemoveByProductId(int id, int number)
        {


            List<Picture> lista = new List<Picture>();
            lista = Context.Pictures.Where(i => i.ProductId == id).ToList();
            Picture p = null;

            if (number == 1)
            {
                if (lista.Count >= 1)
                {
                    p=Context.Pictures.Remove(lista.ElementAt(0));
                }
            }
            else if (number == 2)
            {
                if (lista.Count >= 2)
                {
                    p=Context.Pictures.Remove(lista.ElementAt(1));
                }
            }
            else
            {
                if (lista.Count >= 3)
                {
                    p=Context.Pictures.Remove(lista.ElementAt(2));
                }
            }
            return p;

           
        }

        /// <summary>
        /// Metodo que retorna la ruta de la imagen de un producto
        /// </summary>
        /// <param name="productId">Id del producto</param>
        /// <param name="numero">Numero de la imagen</param>
        /// <returns> Ruta de la imagen</returns>
        public string GetRoute(int productId, int numero)
        {

            List<Picture> lista = new List<Picture>();
            lista = Context.Pictures.Where(i => i.ProductId == productId).ToList();
            string imagen = "";

            if (numero == 1)
            {
                if (lista.Count >= 1)
                {
                    imagen = lista.ElementAt(0).Route;
                }
            }
            else if (numero == 2)
            {
                if (lista.Count >= 2)
                {
                    imagen = lista.ElementAt(1).Route;
                }
            }
            else
            {
                if (lista.Count >= 3)
                {
                    imagen = lista.ElementAt(2).Route;
                }
            }
            return imagen;
        }

    }
}
