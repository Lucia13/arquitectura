﻿using Muebles.CORE;
using Muebles.DAL;
using System.Linq;


namespace Muebles.Application
{

    /// <summary>
    /// Clase para manejar los productos pedidos
    /// </summary>
    public class OrderProductManager: Manager<OrderProduct>, IOrderProductManager
    {

        /// <summary>
        /// Constructor del manager de productos pedidos
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public OrderProductManager(ApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo que retorna los productos del pedido con el id pasado por parámetro
        /// </summary>
        ///  <param name="orderId">Id del pedido</param>
        /// <returns>Iqueryable de productos del pedido</returns>
        public IQueryable<OrderProduct> GetOPByOrderId(int orderId)
        {
            return Context.OrderProducts.Where(i => i.OrderId == orderId);
        }

        /// <summary>
        /// Metodo que retorna la línea de pedido con el id pasado por parámetro
        /// </summary>
        /// <param name="id">Id de la línea de pedido</param>
        /// <returns>Iqueryable de items del pedido</returns>
        public OrderProduct GetById(int id)
        {
            return Context.OrderProducts.Where(i => i.Id == id).SingleOrDefault();
        }

        /// <summary>
        /// Metodo que elimina la línea de pedido del pedido con el id pasado por parámetro
        /// </summary>
        /// <param name="id">Id la línea de pedido</param>
        /// <returns>La línea de pedido eliminada</returns>
        public OrderProduct RemoveById(int id)
        {
            return Context.OrderProducts.Remove(Context.OrderProducts.Where(i => i.Id == id).SingleOrDefault());
        }

       


    }
}
