﻿using Muebles.CORE;
using Muebles.DAL;


namespace Muebles.Application
{

    /// <summary>
    /// Clase para manejar los logs
    /// </summary>
    public class LogManager : Manager<Log>
    {

        /// <summary>
        /// Constructor del manager de logs
        /// </summary>
        /// <param name="context"></param>
        public LogManager(ApplicationDbContext context) : base(context)
        {

        }
    }
}
