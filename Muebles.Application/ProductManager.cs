using Muebles.CORE;
using Muebles.DAL;
using System.Linq;


namespace Muebles.Application
{
    /// <summary>
    /// Clase para manejar los productos
    /// </summary>
    public class ProductManager : Manager<Product>, IProductManager
    {
        /// <summary>
        /// Constructor del manager de productos
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public ProductManager(ApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo que retorna el producto con el id pasado por par�metro
        /// </summary>
        /// <param name="Id">Id del producto</param>
        /// <returns>Producto con el id pasado por par�metro</returns>
        public Product GetById(int id)
        {
            return Context.Products.Where(i => i.Id == id).SingleOrDefault();
        }


        /// <summary>
        /// Metodo que elimina el producto con el id pasado por par�metro
        /// </summary>
        /// <param name="id">Id del producto</param>
        /// <returns>Producto eliminado</returns>
        public Product RemoveById(int id)
        {
            return Context.Products.Remove(Context.Products.Where(i => i.Id == id).SingleOrDefault());
        }

        /// <summary>
        /// Metodo que retorna los productos con stock
        /// </summary>
        /// <returns> IQueryable de productos </returns>

        public IQueryable<Product> GetAllWithStock()
        {
            return Context.Products.Where(i => i.Stock > 0);
        }

        /// <summary>
        /// Metodo que retorna los productos sin stock
        /// </summary>
        /// <returns> IQueryable de productos </returns>
        public IQueryable<Product> GetAllWithoutStock()
        {
            return Context.Products.Where(i => i.Stock == 0);
        }

        /// <summary>
        /// Metodo que retorna todos los productos
        /// </summary>
        /// <returns> IQueryable de productos </returns>
        public IQueryable<Product> GetAll()
        {
            return Context.Products;
        }


        /// <summary>
        /// Metodo que retorna todos los productos de la categor�a pasada por par�metro
        /// </summary>
        /// <param name="categoria">Categor�a de los productos</param>
        /// <returns> IQueryable de productos </returns>
        public IQueryable<Product> GetAllByCategory(string categoria)
        {
            switch (categoria)
            {
                case "cocina":
                    return Context.Products.Where(i => i.Category == Category.Cocina && i.Stock > 0);
                case "salon":
                    return Context.Products.Where(i => i.Category == Category.Sal�n && i.Stock > 0);
                case "dormitorio":
                    return Context.Products.Where(i => i.Category == Category.Dormitorio && i.Stock > 0);
                default:
                    return null;
            }
        }

        /// <summary>
        /// M�todo que convierte una Category de producto en un string 
        /// </summary>
        /// <param name="category">Pedido</param>
        /// <returns">categor�a string</param>
        public string ToStringCategory(Category category)
        {
            string categoria = "";
            if (category == Category.Cocina) categoria = "cocina";
            else if (category == Category.Sal�n) categoria = "salon";
            else if (category == Category.Dormitorio) categoria = "dormitorio";
            return categoria;
        }


    }
}
