using Muebles.CORE;
using Muebles.DAL;
using System;
using System.Linq;

/// <summary>
/// Clase para manejar los pedidos
/// </summary>

namespace Muebles.Application
{
    public class OrderManager : Manager<Order>, IOrderManager
    {

        /// <summary>
        /// Constructor del manager de pedidos
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public OrderManager(ApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo que retorna el pedido con el id pasado por par�metro
        /// </summary>
        /// <param name="id">Id del pedido</param>
        /// <returns>Pedido</returns>
        public Order GetById(int id)
        {
            return Context.Orders.Where(i => i.Id == id).SingleOrDefault();
        }

        /// <summary>
        /// Metodo que retorna los pedidos  de cierto usuario
        /// </summary>
        /// <param name="userId">Id del usuario</param>
        /// <returns> IQueryable de pedidos </returns>
        public IQueryable<Order> GetAllByUserId(string userId)
        {
            return Context.Orders.Where(i => i.User_Id == userId);
        }

        /// <summary>
        /// Metodo que retorna el �ltimo pedido no pagado del usuario
        /// </summary>
        /// <param name="userId">Id del usuario</param>
        /// <returns>Pedido</returns>
        public Order GetByUserIdTodayPending(string userId)
        {
            return Context.Orders.Where(i => i.User_Id == userId && i.CreatedDate.Day==DateTime.Now.Day && i.Status == Status.Pendiente).FirstOrDefault();
        }


        /// <summary>
        /// Metodo que retorna el �ltimo pedido pagado del usuario 
        /// </summary>
        /// <param name="userId">Id del usuario</param>
        /// <returns>Pedido</returns>
        public Order GetByUserIdTodayPayed(string userId)
        {
            return Context.Orders.Where(i => i.User_Id == userId && i.CreatedDate.Day == DateTime.Now.Day && i.Status == Status.Pagado).FirstOrDefault();
        }

        /// <summary>
        /// Metodo que elimina el pedido con el id pasado por par�metro
        /// </summary>
        /// <param name="id">Id del pedido</param>
        /// <returns>Pedido eliminado</returns>
        public Order RemoveById(int id)
        {
            return Context.Orders.Remove(Context.Orders.Where(i => i.Id == id).SingleOrDefault());
        }

        /// <summary>
        /// Metodo que retorna todos los pedidos
        /// </summary>
        /// <returns> IQueryable de productos </returns>
        public IQueryable<Order> GetAll()
        {
            return Context.Orders;
        }





    }
}
