﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Muebles.CORE
{

    /// <summary>
    /// Clase de dominio de pedidos
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Identificador del pedido
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Fecha de pedido
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Usuario que ha creado el pedido
        /// </summary>
        public ApplicationUser User { get; set; }
        [ForeignKey("User")]
        public string User_Id { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Total  a pagar
        /// </summary>
        public float Total { get; set; }
    }
}
