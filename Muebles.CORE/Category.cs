﻿

namespace Muebles.CORE
{

    /// <summary>
    /// Enumerado de categorías de productos
    /// </summary>
    public enum Category : int
    {
        Cocina = 1,
        Salón = 2,
        Dormitorio = 3
    }
}
