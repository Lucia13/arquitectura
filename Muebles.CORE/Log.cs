﻿using System;

namespace Muebles.CORE
{

    /// <summary>
    /// Clase de registro de excepciones
    /// </summary>
    public class Log
    {

        /// <summary>
        /// Identificador 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Fecha
        /// </summary>
        public DateTime Date { get; set; }


        /// <summary>
        /// Descripción
        /// </summary>
        public string Message { get; set; }

  
    }
}
