﻿
namespace Muebles.CORE
{
    /// <summary>
    /// Enumerado de estados de pedido
    /// </summary>
    public enum Status : int
        {
            Pendiente = 1,
            Pagado = 2,
            Enviado = 3,
            Recibido = 4,
            Cancelado = 5
        }
    }

