﻿

namespace Muebles.CORE
{
    /// <summary>
    /// Clase de dominio de productos
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Stock del producto
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripción del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// Categoria del producto
        /// </summary>
        public Category Category { get; set; }

        


    }
}
