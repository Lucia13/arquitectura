﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace Muebles.CORE
{
    /// <summary>
    /// Clase de dominio de direcciones
    /// </summary>
    public class Address
    {

        /// <summary>
        /// Identificador de la dirección
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Dirección
        /// </summary>
        public string Street { get; set; }


        /// <summary>
        /// Ciudad
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Provincia
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Código postal
        /// </summary>
        public string CP { get; set; }

        /// <summary>
        /// País
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Id del usuario al que pertenece
        /// </summary>
        public ApplicationUser ApplicationUser { get; set; }
        [ForeignKey("ApplicationUser")]
        public string User_Id { get; set; }
    }
}
