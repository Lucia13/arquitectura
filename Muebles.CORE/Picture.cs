﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace Muebles.CORE
{
    /// <summary>
    /// Clase de dominio de imágenes
    /// </summary>
    public class Picture
    {
        /// <summary>
        /// Identificador de la imagen
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ruta de la imagen
        /// </summary>
        public string Route { get; set; }

        /// <summary>
        /// Id del producto al que pertenece
        /// </summary>
        public Product Product { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }

    }
}
