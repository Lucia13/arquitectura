﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Muebles.CORE
{
    /// <summary>
    /// Clase de productos en el pedido
    /// </summary>
    public class OrderProduct
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id del pedido
        /// </summary>
        public Order Order { get; set; }
        [ForeignKey("Order")]
        public int OrderId { get; set; }

        /// <summary>
        /// Id del producto
        /// </summary>
        public Product Product { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        /// <summary>
        /// Cantidad
        /// </summary>
        public int Number { get; set; }
    }
}
