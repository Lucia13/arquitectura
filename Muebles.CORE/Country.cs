﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Muebles.CORE
{
    /// <summary>
    /// Enumerado de países de los clientes
    /// </summary>
    public enum Country : int
    {
        España = 1,
        Portugal = 2,
        Andorra = 3,
        Francia = 4
    }
}
