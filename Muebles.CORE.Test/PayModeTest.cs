﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Muebles.IFR.PayModes;

namespace Muebles.IFR.Test
{

    ///<summary>
    /// Prueba de los métodos de las clases del paquete PayModes
    /// </summary>
    [TestClass]
    public class PayModeTest
    {

        ///<summary>
        /// Prueba de si el método Pay comprueba validez de los datos de la tarjeta de crédito correctamente
        /// </summary>
        [TestMethod]
        public void CreditCardTest()
        {
            CreditCard card = new CreditCard();
            Assert.IsTrue(card.Pay("Lucia", "13", 133, "03/23"));
        }

        ///<summary>
        /// Prueba de si el método Pay comprueba validez de los datos de la tarjeta de dñebito correctamente
        /// </summary>
        [TestMethod]
        public void DebitCardTest()
        {
            DebitCard card = new DebitCard();
            Assert.IsTrue(card.Pay("Nil", "31", 331, "03/24"));
        }

        ///<summary>
        /// Prueba de si el método Pay permite pagar vía PayPal
        /// </summary>
        [TestMethod]
        public void PayPalTest()
        {
            PayPal paypal = new PayPal();
            Assert.IsTrue(paypal.Pay("null", "null", 0, "null"));
        }
    }
}
