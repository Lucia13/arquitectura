﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Muebles.MVC.Startup))]
namespace Muebles.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
