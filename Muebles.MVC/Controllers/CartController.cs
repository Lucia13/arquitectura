﻿using Muebles.Application;
using System.Collections.Generic;
using System.Linq;
using Muebles.CORE;
using System.Web.Mvc;
using Muebles.MVC.Models;
using Microsoft.AspNet.Identity;
using System;
using Muebles.IFR;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión del carrito de la compra
    /// </summary>
    public class CartController : Controller
    {

        readonly IOrderProductManager orderProductManager;
        readonly IOrderManager orderManager;
        readonly IProductManager productManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempOrderProduct">manager de líneas de pedido</param>
        /// <param name="tempOrder">manager de pedidos</param>
        /// <param name="tempProduct">manager de productos</param>
        public CartController(IOrderProductManager tempOrderProduct, IOrderManager tempOrder, IProductManager tempProduct)
        {
            this.orderProductManager = tempOrderProduct;
            this.orderManager = tempOrder;
            this.productManager = tempProduct;
        }

        public CartController() { }

        /// <summary>
        /// Método que muestra todas las líneas de pedido en el carrito del usuario logeado
        /// </summary>
        /// <returns>Resultado de la acción</returns>

        public ActionResult Index()
        {
            #region Inicializacón de las variables
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();

            List<OrderProduct> listaOP = new List<OrderProduct>();
            Order pedido = new Order();
            #endregion

            try
            {
                //buscamos el pedido del usuario
                pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId());

                if (pedido != null) //Si el pedido existe
                {
                    //buscamos las lineas del pedido
                    listaOP = orderProductManager.GetOPByOrderId(pedido.Id).ToList();
                    if (listaOP.Count != 0)
                    {
                        //Creamos un modelo de vista a partir de los datos la de línea de pedido
                        var lineas = listaOP.Select(i => new CartViewModel()
                        {
                            Id = i.Id,
                            ProductId = i.ProductId, //Id de producto
                            ProductName = productManager.GetById(i.ProductId).Name, //Nombre de producto
                            ProductPrice = productManager.GetById(i.ProductId).Price, //Precio unitario de producto
                            Number = i.Number, //Cantidad pedida
                            Price = productManager.GetById(i.ProductId).Price * i.Number //Precio unitario x cantidad pedida
                        });
                        ViewBag.Total = pedido.Total; //Mostramos el total

                        return View(lineas);//devolvemos el modelo de vista
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }



        /// <summary>
        /// Método que resta o suma un item a la línea de pedido
        /// </summary>
        /// <param name="id">Id de la línea de pedido</param>
        /// <param name="sumar">Determina si sumamos o restamos un item, verdadero suma, falo resta</param>
        /// <returns>Resultado de la acción</returns>

        public ActionResult EditSumarRestar(int id, bool sumar)
        {

            try
            {
                // TODO: Add update logic here
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
                OrderProduct op = orderProductManager.GetById(id); //Obtenemos la línea de pedido a partir de su id
                Order pedido = orderManager.GetById(op.OrderId); //obtenemos el pedido al que pertenece la línea de padido a partir de su id
                Product producto = productManager.GetById(op.ProductId); //obtenemos el producto de la línea de padido a partir de su id

                if (!sumar) //Si restamos
                {
                    if (op.Number >= 1) //Podemos restar solo si hay al menos un item

                    {
                        op.Number--; //disminuimos la cantidad
                        pedido.Total -= producto.Price; //disminuimos el total del pedido
                        context.SaveChanges(); //guardamos los cambios
                    }
                }
                else //si sumamos
                {
                    op.Number++; //aumentamos la cantidad
                    pedido.Total += producto.Price; //aumentamos el total del pedido
                    context.SaveChanges(); //guardamos los cambios
                }
                return RedirectToAction("Index"); //Actualizamos la página del carrito

            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }



        /// <summary>
        /// Método que elimina una línea de pedido del pedido
        /// </summary>
        /// <param name="id">Id de la línea de pedido</param>
        /// <returns>Resultado de la acción</returns>

        public ActionResult DeleteItem(int id)
        {
            try
            {

                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos

                OrderProduct item = orderProductManager.GetById(id); //Obtenemos la llínea de pedido a partir de su id

                Order pedido = orderManager.GetById(item.OrderId); //obtenemos el pedido al que pertenece la línea de padido a partir de su id

                Product producto = productManager.GetById(item.ProductId); //obtenemos el producto de la línea de padido a partir de su id

                pedido.Total -= (item.Number * producto.Price); //disminuimos el total del pedido

                orderProductManager.RemoveById(id); //eliminamos la línea de pedido
                context.SaveChanges(); //guardamos los cambios

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

    }
}
