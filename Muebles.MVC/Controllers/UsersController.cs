﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Muebles.Application;
using Muebles.CORE;
using Muebles.IFR;
using Muebles.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión de usuarios
    /// </summary>
    public class UsersController : Controller
    {

        readonly IAddressManager addressManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempAddress">manager de direcciones</param>
        public UsersController(IAddressManager tempAddress)
        {
            this.addressManager = tempAddress;
        }

        public UsersController() { }

        /// <summary>
        /// Método para mostrar todos los usuarios
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Index()
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios
            var contextIdentity = new IdentityDbContext();
            AddressManager addressMananager = new AddressManager(context);

            try
            {
                List<IdentityUser> lista = contextIdentity.Users.ToList(); //lista de todos los usuarios
                if (lista.Count != 0)
                {
                    //Si existen usuarios, creamos un modelo de vista con los datos de los usuarios
                    var users = lista.Select(user => new UsersViewModel()
                    {

                        Id = user.Id,
                        Name = manager.FindById(user.Id).Name,
                        Surnames = manager.FindById(user.Id).Surnames,
                        Street = addressMananager.GetByUser(user.Id).Street,
                        City = addressMananager.GetByUser(user.Id).City,
                        Province = addressMananager.GetByUser(user.Id).Province,
                        CP = addressMananager.GetByUser(user.Id).CP,
                        Country = addressMananager.GetByUser(user.Id).Country,
                        Email = user.Email,
                        Phone = user.PhoneNumber
                    });
                    return View(users); //devolvemos el modelo de vista
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para mostrar los detalles del usuario con el id pasado por parámetro
        /// <param name="userId">Id del usuario</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Details(string userId)
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            try
            {
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios
                Models.ApplicationUser user = manager.FindById(userId); //obtenemos al usuario con el id pasado por parámetro
                Address address = addressManager.GetByUser(userId); //dirección del usuario

                if (user != null) //si el usuario no es nulo
                {
                    //Creamos un modelo de vista con los datos del usuario
                    UsersViewModel vm = new UsersViewModel()
                    {
                        Id = user.Id,
                        Name = user.Name,
                        Surnames = user.Surnames,
                        Street = address.Street,
                        City = address.City,
                        Province = address.Province,
                        CP = address.CP,
                        Country = address.Country,
                        Email = user.Email,
                        Phone = user.PhoneNumber
                    };
                    return View(vm); //devolvemos el modelo de vista
                }
                else
                {
                    return RedirectToAction("Index"); //abrimos la página de la lista de usuarios
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }


        /// <summary>
        /// Método para mostrar los detalles del usuario que se desea modificar
        /// <param name="userId">Id del usuario</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Edit(string userId)
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();

            try
            {
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios
                Models.ApplicationUser user = manager.FindById(userId); //obtenemos al usuario con el id pasado por parámetro
                Address address = addressManager.GetByUser(userId); //dirección del usuario

                if (user != null) //si el usuario no es nulo
                {
                    //Creamos un modelo de vista con los datos del usuario
                    UsersViewModel vm = new UsersViewModel()
                    {
                        Name = user.Name,
                        Surnames = user.Surnames,
                        Street = address.Street,
                        City = address.City,
                        Province = address.Province,
                        CP = address.CP,
                        Country = address.Country,
                        Email = user.Email,
                        Phone = user.PhoneNumber
                    };
                    return View(vm); //devolvemos el modelo de vista
                }
                else
                {
                    return RedirectToAction("Index"); //abrimos la página de la lista de usuarios
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }

        }

        /// <summary>
        /// Método para guardar los cambios en los datos del usuario
        /// <param name="userId">Id del usuario</param>
        /// <param name="i">Modelo de la vista de datos del usuario</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Edit(string userId, UsersViewModel i)
        {

            try
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
                UserManager<CORE.ApplicationUser> userManager = new UserManager<CORE.ApplicationUser>(new UserStore<CORE.ApplicationUser>(context)); //manager de usuarios
                CORE.ApplicationUser user = userManager.Users.Where(u => u.Id == userId).Single(); //buscamos al usuario con el id pasado por parámetro
                                                                                                   //var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();  //manager de usuarios
                                                                                                   // user = manager.FindById(userId);
                Address address = addressManager.GetByUser(userId); //dirección del usuario

                //actualizamos los datos del usuario
                user.Name = i.Name;
                user.Surnames = i.Surnames;
                address.Street = i.Street;
                address.City = i.City;
                address.Province = i.Province;
                address.CP = i.CP;
                address.Country = i.Country;
                user.Email = i.Email;
                user.PhoneNumber = i.Phone;
                IdentityResult result = userManager.Update(user);
                context.SaveChanges();

                return RedirectToAction("Index"); //abrimos la página de la lista de usuarios
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

    }
}
