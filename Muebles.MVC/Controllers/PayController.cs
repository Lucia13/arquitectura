﻿using Muebles.Application;
using System;
using Muebles.CORE;
using Muebles.MVC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Muebles.IFR;
using Muebles.IFR.PayModes;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión de pago
    /// </summary>
    public class PayController : Controller
    {
        /// <summary>
        /// Método para mostrar el formulario de pago
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Create()
        {
            return View();
        }


        /// <summary>
        /// Método de gestión del pago
        /// <param name="i">Modelo de la vista de la línea de pedido</param>
        /// <param name="CreditCard">Pago con la tarjeta de crédito/param>
        /// <param name="DebitCard">Pago con la tarjeta de débito</param>
        /// <param name="PayPal">Pago con Paypal</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Create(PayViewModel i, string creditCard, string debitCard, string payPal)
        {
            #region Inicialización de las variables
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
            List<OrderProduct> listaOP = new List<OrderProduct>(); //lista d elíneas de pedido
            Order pedid = null; //pedido
            float total = 0.0f; //total del pedido
            #endregion
            bool correcto = false; //Datos de pago son correctos?

            try
            {
                //Tarjeta de crédito
                if (!string.IsNullOrEmpty(creditCard))
                {
                    var cliente = new Client(new CreditCard(), i.Owner, i.Number, i.CCV, i.Expiration);
                    //Comprobamos la validez de los datos de la tarjeta de crédito
                    if (cliente.Pay(i.Owner, i.Number, i.CCV, i.Expiration))
                    { correcto = true; }
                }
                //Tarjeta de débito
                else if (!string.IsNullOrEmpty(debitCard))
                {
                    var cliente = new Client(new DebitCard(), i.Owner, i.Number, i.CCV, i.Expiration);
                    //Comprobamos la validez de los datos de la tarjeta de crédito
                    if (cliente.Pay(i.Owner, i.Number, i.CCV, i.Expiration))
                    { correcto = true; }
                }
                //Paypal
                else if (!string.IsNullOrEmpty(payPal))
                {
                    var cliente = new Client(new PayPal(), i.Owner, i.Number, i.CCV, i.Expiration);
                    //Comprobamos la validez de los datos de la tarjeta de crédito
                    if (cliente.Pay(null, null, 0, null))
                    { correcto = true; }
                }

                if (correcto)
                {

                    OrderProductManager opManager = new OrderProductManager(context); //contexto de datos
                    ProductManager producManager = new ProductManager(context); //manager de productos
                    OrderManager orderManager = new OrderManager(context);  //manager de pedidos

                    pedid = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId()); //recuperamos el pedido pendiente

                    if (pedid != null)
                    {
                        Product producto = new Product();

                        //obtenemos las lineas del pedido
                        listaOP = opManager.GetOPByOrderId(pedid.Id).ToList();

                        //recorremos la lista de las líneas de pedido para actualizar el total del pedido y y disminuir el stock de los productos
                        foreach (OrderProduct op in listaOP)
                        {
                            producto = producManager.GetById(op.ProductId);

                            //calculamos el total del pedido
                            total += (producto.Price * op.Number);
                            //bajamos el stock de los productos comprados
                            producto.Stock -= op.Number;

                        }
                        pedid.Total = total; //actualizamos el total del pedido
                        context.SaveChanges(); //Guardamos los cambios

                        return RedirectToAction("Index", "Resume"); //Abrimos la página del resumen del pedido
                    }
                    else { return View(); }
                }
                else
                {
                    TempData["Fail"] = "Datos de pago incorrectos."; //Mostramos un mensaje de fallo
                    return RedirectToAction("Create", "Pay");
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

    }
}