﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Muebles.MVC.Controllers.Admin
{
    /// <summary>
    /// Clase de gestión de las páginas de ayuda
    /// </summary>
    public class HelpController : Controller
    {
        /// <summary>
        /// Método para mostrar la ayuda del administrador
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult HelpAdmin()
        {
            return View();
        }

        /// <summary>
        /// Método para mostrar la ayuda del usuario
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult HelpUser()
        {
            return View();
        }

    }
}