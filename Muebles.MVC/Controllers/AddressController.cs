﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Muebles.Application;
using Muebles.CORE;
using Muebles.IFR;
using Muebles.MVC.Models;
using System;
using System.Web;
using System.Web.Mvc;

namespace Muebles.MVC.Controllers
{

    /// <summary>
    /// Clase controladora de la modificación de datos de entrega
    /// </summary>
    public class AddressController : Controller
    {

        /// <summary>
        /// Método que muestra los datos de entrega del usuario
        /// </summary>
        /// <returns>Resultado de la acción</returns>
        public ActionResult Edit()
        {
            
            try
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios
                string UserId = User.Identity.GetUserId(); //Obtenemos el id del usuario logeado
                Models.ApplicationUser user = manager.FindById(UserId); //Obtenemos al usuario a partir de su id
                AddressManager addressMananager = new AddressManager(context);
                Address address = addressMananager.GetByUser(UserId); //dirección del usuario

                //Creamos un modelo de vista a partir de los datos del usuario
                AddressViewModel vm = new AddressViewModel()
                {
                    Name = user.Name,
                    Surnames = user.Surnames,
                    Street = address.Street,
                    City = address.City,
                    Province = address.Province,
                    CP = address.CP,
                    Country = address.Country,
                    Email = user.Email,
                    Phone = user.PhoneNumber
                };
                return View(vm); //devolvemos el modelo de vista a la vista para que muestre los datos de entrega

            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }

        }


        /// <summary>
        /// Método que actualiza los datos de entrega del usuario
        /// <param name="i">Modelo de datos de entrega</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Edit(AddressViewModel i)
        {

            try
            {
                // TODO: Add update logic here
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();  //manager de usuarios
                string UserId = User.Identity.GetUserId(); //Obtenemos el id del usuario logeado
                Models.ApplicationUser user = manager.FindById(UserId); //Obtenemos al usuario a partir de su id
                AddressManager addressMananager = new AddressManager(context);
                Address address = addressMananager.GetByUser(UserId); //dirección del usuario

                //asignamos los valores modificados al usuario
                user.Name = i.Name;
                user.Surnames = i.Surnames;
               address.Street = i.Street;
               address.City = i.City;
               address.Province = i.Province;
               address.CP = i.CP;
               address.Country = i.Country;
                user.Email = i.Email;
                user.PhoneNumber = i.Phone;

                //actualizamos el contexto de datos
                IdentityResult result = manager.Update(user);
                context.SaveChanges();

                return RedirectToAction("Create", "Pay");

            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }

        }
    }
}
