﻿using Muebles.Application;
using Muebles.CORE;
using Muebles.MVC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using Muebles.IFR;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión de pedidos de un determinado usuario
    /// </summary>
    public class UserOrderController : Controller
    {

        readonly IOrderManager orderManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempOrder">manager</param>
        public UserOrderController(IOrderManager tempOrder)
        {
            this.orderManager = tempOrder;
        }

        public UserOrderController() { }

        /// <summary>
        /// Método para mostrar todos los pedidos de usuario logeado
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Index()
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios

            try
            {
                List<Order> lista = orderManager.GetAllByUserId(User.Identity.GetUserId()).ToList(); //lista de todos los pedidos del usuario logeado

                if (lista.Count != 0) // si la lista no esta vacía
                {
                    //Creamos un modelo de vista con los datos de los pedidos del usuario
                    var pedidos = lista.Select(i => new UserOrderViewModel()
                    {
                        Id = i.Id,
                        CreatedDate = i.CreatedDate,
                        Status = i.Status,
                        Total = i.Total
                    });

                    return View(pedidos); //devolvemos el modelo de vista
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

    }
}
