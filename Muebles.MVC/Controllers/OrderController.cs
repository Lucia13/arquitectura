﻿using Muebles.Application;
using System;
using Muebles.CORE;
using Muebles.MVC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Muebles.IFR;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión de pedidos
    /// </summary>
    public class OrderController : Controller
    {

        readonly IOrderManager orderManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempOrder">manager</param>
        public OrderController(IOrderManager tempOrder)
        {
            this.orderManager = tempOrder;
        }

        public OrderController() { }

        /// <summary>
        /// Método para mostrar todos los pedidos
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Index()
        {

            try
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios

                List<Order> lista = orderManager.GetAll().ToList(); //obtenemos lista de todos los pedidos 

                if (lista.Count != 0) //si existen pedidos
                {
                    //Creamos un modelo de vista con los datos de los pedidos
                    var pedidos = lista.Select(i => new OrderViewModel()
                    {
                        Id = i.Id,
                        CreatedDate = i.CreatedDate,
                        Client = i.User_Id,
                        ClientName = manager.FindById(i.User_Id).Name + " " + manager.FindById(i.User_Id).Surnames,
                        Status = i.Status,
                        Total = i.Total
                    });

                    return View(pedidos); //devolvemos el modelo de vista
                }
                else
                {
                    return View(); //si la lista está vacía, no devolvemos nada
                }
            }
            catch (NullReferenceException ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }


        }

        /// <summary>
        /// Método para mostrar los detalles del pedido con el id pasado por parámetro
        /// <param name="id">Id del pedido</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Details(int id)
        {
            try
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios

                Order i = orderManager.GetById(id); //obtenemos el pedido con el id recibido por parámetro

                //si el pedido no es nulo creamos un modelo de vista con los datos del pedido
                if (i != null)
                {
                    OrderViewModel vm = new OrderViewModel()
                    {
                        Id = i.Id,
                        Client = i.User_Id,
                        ClientName = manager.FindById(i.User_Id).Name + " " + manager.FindById(i.User_Id).Surnames,
                        Status = i.Status,
                        Total = i.Total
                    };
                    return View(vm); //devolvemos el modelo de vista
                }
                else
                {
                    return RedirectToAction("Create"); //si no existe pedido, abrimos la página creación de un nuevo pedido
                }
            }
            catch (NullReferenceException ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para mostrar el formulario de creación de pedido
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Método para guardar los datos del nuevo pedido
        /// <param name="i">Modelo de la vista de datos del pedido</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Create(OrderViewModel i)
        {
            try
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();  //contexto de datos
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios
                Models.ApplicationUser user = manager.FindByEmail(i.Client); //Buscamos al usuario con el nombre de usuario indicado

                //Creamos un nuevo pedido
                Order or = new Order()
                {
                    Id = i.Id,
                    CreatedDate = DateTime.Now,
                    User_Id = i.Client,
                    Status = i.Status,
                    Total = 0

                };

                //guardamos el pedido al contexto de datos
                orderManager.Add(or);
                context.SaveChanges();

                return RedirectToAction("Index"); //abrimos la página de la lista de pedidos

            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para mostrar los datos del pedido que se desea modificar
        /// <param name="id">Id del pedido a modificar</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Edit(int id)
        {

            try
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();  //contexto de datos
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios

                Order i = orderManager.GetById(id); //obtenemos el pedido con el id pasado por parémtro
                if (i != null)
                {
                    //Creamos un modelo de vista con los datos del pedido
                    OrderViewModel vm = new OrderViewModel()
                    {
                        Id = i.Id,
                        Client = i.User_Id,
                        ClientName = manager.FindById(i.User_Id).Name + " " + manager.FindById(i.User_Id).Surnames,
                        Status = i.Status,
                        Total = i.Total
                    };
                    return View(vm); // devolvemos el modelo de vista
                }
                else
                {
                    return RedirectToAction("Create"); //Si el pedido no existe, abrimos la pagina de creación de pedido
                }
            }
            catch (NullReferenceException ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para guardar los cambios en los datos del pedido
        /// <param name="id">Id del pedido</param>
        /// <param name="i">Modelo de la vista de datos del pedido</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Edit(int id, OrderViewModel i)
        {

            try
            {
                var manager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); //manager de usuarios
                Models.ApplicationUser user = manager.FindByEmail(i.Client); //Buscamos al usuario con el nombre de usuario indicado
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();  //contexto de datos

                Order p = orderManager.GetById(id); //obtenemos el pedido con el id pasado por parémtro

                //le asignamos los valores modificados
                p.Id = i.Id;
                p.Status = i.Status;
                p.Total = i.Total;

                context.SaveChanges(); //guardamos los cambios

                //si se cancela el pedido, devolvemos los productos al almacén - el stock aumenta
                if (p.Status == Status.Cancelado)
                {
                    ProductManager productManager = new ProductManager(context); //manager de productos
                    OrderProductManager opManager = new OrderProductManager(context); //manager de líneas de pedido
                    List<OrderProduct> opLista = opManager.GetOPByOrderId(p.Id).ToList(); //obtenemos la lista de las líneas del pedido cancelado
                    foreach (OrderProduct op in opLista) //para cada  línea de pedido
                    {
                        Product producto = productManager.GetById(op.ProductId); //obtenemos el producto cuyo stock vamos a aumentar
                        producto.Stock += op.Number; //aumentamos el stock
                    }
                    context.SaveChanges(); //guardamos los cambios
                }
                return RedirectToAction("Index"); //Abrimos la página de la lista de pedidos
            }

            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método que elimina un pedido
        /// </summary>
        /// <param name="id">Id del pedido</param>
        /// <returns>Resultado de la acción</returns>

        public ActionResult DeleteItem(int id)
        {
            try
            {

                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
                Order pedido = orderManager.GetById(id); //obtenemos el pedido al que pertenece la línea de padido a partir de su id

                orderManager.RemoveById(id); //eliminamos la línea de pedido
                context.SaveChanges(); //guardamos los cambios

                TempData["Success"] = "El pedido sa ha eliminado con éxito.";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }



    }
}
