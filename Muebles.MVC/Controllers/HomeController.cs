﻿
using System.Web.Mvc;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión de las páginas de inicio y contacto
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Método para mostrar la página de inicio
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Método para mostrar la página de contacto
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Contact()
        {

            return View();
        }
    }
}