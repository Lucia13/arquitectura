﻿using Muebles.Application;
using System.Collections.Generic;
using System.Linq;
using Muebles.CORE;
using System.Web.Mvc;
using Muebles.MVC.Models;
using System;
using Muebles.IFR;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión de líneas de pedidos de un determinado usuario
    /// </summary>
    public class UserOrderProductController : Controller
    {

        readonly IOrderProductManager orderProductManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempOrderProduct">manager</param>
        public UserOrderProductController(IOrderProductManager tempOrderProduct)
        {
            this.orderProductManager = tempOrderProduct;
        }

        public UserOrderProductController() { }


        /// <summary>
        /// Método para mostrar todas las líneas de un pedido 
        /// <param name="id">Id del pedido</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Index(int id)
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            ProductManager productManager = new ProductManager(context);

            try
            {
                //obtenemos la lista de líneas de pedido del pedido con el id pasado por parámetro
                List<OrderProduct> lista = orderProductManager.GetOPByOrderId(id).ToList(); 
                if (lista.Count != 0) //si la lista no está vacía
                {
                    //Creamos un modelo de vista con los datos de las líneas de pedido
                    var lineas = lista.Select(i => new OrderProductViewModel()
                    {
                        Id = i.Id,
                        ProductId = i.ProductId,
                        ProductName = productManager.GetById(i.ProductId).Name, //Nombre del producto
                        ProductPrice = productManager.GetById(i.ProductId).Price, //Precio unitario del producto
                        Number = i.Number, //Cantidad pedida
                        Price = productManager.GetById(i.ProductId).Price * i.Number //Precio
                    });
                    return View(lineas); //devolvemos el modelo de vista
                }
                else
                {
                    return View();
                }

            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }


    }
}
