﻿using Muebles.Application;
using Muebles.CORE;
using Muebles.MVC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Muebles.IFR;
using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Net.Mail;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase para mostrar el resumen del pedido y enviar email con su copia al vendedor y cliente
    /// </summary>
    public class ResumeController : Controller
    {


        readonly IOrderProductManager orderProductManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempOrderProduct">manager de líneas de pedido</param>
        public ResumeController(IOrderProductManager tempOrderProduct)
        {
            this.orderProductManager = tempOrderProduct;
        }

        public ResumeController() { }
       

        /// <summary>
        /// Método para mostrar el reusmen del pedido recién cobrado y enviar email con su copia al vendedor y cliente
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Index()
        {

            #region Inicialización de las variables
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            List<OrderProduct> listaOP = new List<OrderProduct>();
            Order pedido = new Order();
            string articulo = ""; //string que formará parte de email con la copia del pedido
            #endregion

            try
            {
                #region managers
                ProductManager productManager = new ProductManager(context);
                OrderManager orderManager = new OrderManager(context);
                #endregion

                //buscamos el pedido del usuario
                pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId());

                if (pedido != null)
                {
                    pedido.Status = Status.Pagado; //el status del pedido será pagado
                    context.SaveChanges(); //guardamos el cambio

                    //buscamos las líneas del pedido
                    listaOP = orderProductManager.GetOPByOrderId(pedido.Id).ToList();

                    if (listaOP.Count != 0)
                    {
                        //Creamos un modelo de vista con las líneas del pedido
                        var lineas = listaOP.Select(i => new CartViewModel()
                        {
                            Id = i.Id,
                            ProductId = i.ProductId,
                            ProductName = productManager.GetById(i.ProductId).Name,
                            ProductPrice = productManager.GetById(i.ProductId).Price,
                            Number = i.Number,
                            Price = productManager.GetById(i.ProductId).Price * i.Number
                        });

                        ViewBag.Total = pedido.Total; //escribimos el total del pedido

                        Product producto = new Product();

                        foreach (OrderProduct op in listaOP)
                        {
                            producto = productManager.GetById(op.ProductId);
                            //preparamos string con los detalles del pedido que enviaremos por email
                            articulo += producto.Name + " x " + op.Number + " " + (producto.Price * op.Number) + "€ <br />";
                        }

                        //usuario-cliente
                        var userManager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                        Models.ApplicationUser usuario = userManager.Users.Where(u => u.Id == pedido.User_Id).Single();

                        //UserManager<CORE.ApplicationUser> userManager = new UserManager<CORE.ApplicationUser>(new UserStore<CORE.ApplicationUser>(context)); //manager de usuarios
                        //CORE.ApplicationUser usuario = userManager.Users.Where(u => u.Id == pedido.User_Id).Single();

                        AddressManager addressMananager = new AddressManager(context);
                        Address direccion = addressMananager.GetByUser(usuario.Id); //dirección del usuario

                        //Enviamos email al cliente y al vendedor
                        
                       Send(usuario, direccion, pedido, articulo);

                        return View(lineas); //devolvemos el modelo de vista
                    }
                    else
                    {
                        return View();
                    }
                }
                else { return View(); }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }


        /// <summary>
        /// Método de gestión del pago y comprobación de la validez de los datos de la tarjeta de crédito
        /// <param name="usuario">Objeto AplicationUser - cliente al que enviamos el correo</param>
        ///  /// <param name="address">dirección de entrega</param>
        /// <param name="pedido">Objeto User - pedido realizado por el cliente</param>
        /// <param name="articulo">String de items del pedido</param>
        /// </summary>
        public static void Send(Models.ApplicationUser usuario, Address address, Order pedido, string articulo)
        {
            //Enviamos una copia del pedido por correo al cliente y al vendedor
            System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

            //Direcciones de correo electrónico a las que queremos enviar el mensaje
            mmsg.To.Add(new MailAddress("luciadaubnerova13@gmail.com"));  //enviamos una copia al vendedor
            mmsg.To.Add(new MailAddress(usuario.Email)); //enviamos una copia al cliente

            //Correo electronico desde el que enviamos el mensaje
            mmsg.From = new System.Net.Mail.MailAddress("luciadaubnerova13@gmail.com");

            //Asunto
            mmsg.Subject = "MiCasa - Copia del pedido";
            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

            CORE.ApplicationUser user = new CORE.ApplicationUser();
            string fullname = usuario.Name + " " +  usuario.Surnames;

            //Cuerpo del Mensaje - copia del pedido
            mmsg.Body = "<h2>Pedido MiCasa</h2><p>Estimado cliente,</p><p>su pedido se ha realizado con éxito. Recibirá los artículos dentro de los próximos 7 días en la dirección: <br/>" + fullname
                 + "<br />" + address.Street + "<br/>" + address.CP + "<br />" + address.Province + ", " + address.Country + " </p>" + articulo + "<u>Total:</u> " + pedido.Total.ToString() + "€<p>Equipo MiCasa</p>";
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = true; //Queremos que se envíe como HTML


            /*-------------------------CLIENTE DE CORREO----------------------*/

            //Creamos un objeto de cliente de correo
            System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

            //Hay que crear las credenciales del correo emisor
            cliente.UseDefaultCredentials = false;
            cliente.Credentials = new System.Net.NetworkCredential("luciadaubnerova13@gmail.com", "Paulsa13.");

            //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail
            cliente.Port = 587;
            cliente.EnableSsl = true;

            //Para Gmail
            cliente.Host = "smtp.gmail.com";

            /*-------------------------ENVIO DE CORREO----------------------*/
            //Enviamos el mensaje      
            cliente.Send(mmsg);
            mmsg.Dispose();
        }


    }
}