﻿using Muebles.Application;
using Muebles.CORE;
using Muebles.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Muebles.IFR;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión del catálogo de productos
    /// </summary>
    public class CatalogController : Controller
    {
        /// <summary>
        /// Método que muestra los productos de la categoría pasada por parámetro
        /// <param name="i">Modelo de datos de entrega</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>

        readonly IProductManager productManager;
        readonly IPictureManager pictureManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempProduct">manager de productos</param>
        /// <param name="tempPicture">manager de imagenes</param>
        public CatalogController(IProductManager tempProduct, PictureManager tempPicture)
        {
            this.productManager = tempProduct;
            this.pictureManager = tempPicture;
        }

        public CatalogController() { }

        public ActionResult Index(string category)
        {
            try
            {
                #region Inicialización de variables
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
                #endregion

                List<Product> lista = productManager.GetAllByCategory(category).ToList(); //obtenemos una lista de productos de una determinada categoría
                if (lista.Count != 0) //si la lista no está vacía
                {
                    //Creamos un modelo de vista con los datos de los productos
                    var productos = lista.Select(i => new CatalogViewModel()
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Description = i.Description,
                        Price = i.Price,
                        Category = i.Category,
                        Image1 = "~/images/" + pictureManager.GetRoute(i.Id, 1),
                        Image2 = "~/images/" + pictureManager.GetRoute(i.Id, 2),
                        Image3 = "~/images/" + pictureManager.GetRoute(i.Id, 3)
                    });

                    return View(productos); //devolvemos el modelo de vista
                }
                else
                {
                    return View(); // si la lista está vacía, no devolvemos nada
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método que muestra los datos del producto con el id pasado por parámetro
        /// <param name="string">Categoría de  los productos</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Edit(int id, Category category)
        {

            if (User.Identity.GetUserId() != null) //si hay un uusario logeado
            {
                #region Inicialización de variables
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
                OrderManager orderManager = new OrderManager(context);
                OrderProductManager opManager = new OrderProductManager(context);
                Product product = new Product();
                #endregion

                String categoria = productManager.ToStringCategory(category);

                try
                {
                    //buscamos pedidos pendientes del usuario
                    Order pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId());

                    // si existe un pedido no pagado del mismo día
                    if (pedido != null)
                    {
                    }
                    //si no existe pedido no pagado del mismo día, creamos un pedido nuevo
                    else
                    {
                        pedido = new Order()
                        {
                            CreatedDate = DateTime.Now,
                            User_Id = User.Identity.GetUserId(),
                            Status = Status.Pendiente,
                            Total = 0.0f
                        };

                        //Añadimos el pedido al contexto de datos
                        orderManager.Add(pedido);
                        context.SaveChanges();
                    }

                    ////Creamos una nueva línea de pedido
                    OrderProduct op = new OrderProduct();
                    op.OrderId = pedido.Id;
                    op.ProductId = id;
                    op.Number = 1;

                    //Aadimos la línea de pedido al contexto de datos
                    opManager.Add(op);
                    context.SaveChanges();

                    //actualizamos el total del pedido
                    pedido.Total += productManager.GetById(id).Price;
                    context.SaveChanges();

                    TempData["Success"] = "El producto fue añadido al carrito."; //Mostramos un mensaje de éxito
                    return RedirectToAction("Index", new { category = categoria });
                }

                catch (Exception ex)
                {
                    //Guardamos información de la excepción en el archivo de log
                    Log.LogError(ex.Message, ex);
                    return View();
                }
            }
            else //Si el usuario no está logeado abrimos la página de login. El usuario tiene que acceder a su cuenta para poder comprar un producto
            {
                return RedirectToAction("Login", "Account", new { returnUrl = "Login" });
            }
        }

        /// <summary>
        /// Método que permite terminar la compra y pasar al carrito
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult ToCart()
        {
            if (User.Identity.GetUserId() != null) //Si el usuario está logeado
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
                OrderManager orderManager = new OrderManager(context);
                Order pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId()); //Obtenemos su pedido pendiente

                // si existe un pedido no pagado del mismo día, abrimos la pagina de carrito
                if (pedido != null)
                {
                    return RedirectToAction("Index", "Cart");
                }
                else //Si el usuario no ha añadido ningún produto al carrito, lo avisamos
                {
                    TempData["Empty"] = "El carrito de la compra está vacío!";
                    return RedirectToAction("Index", "Catalog", new { category = "cocina" });
                }
            }
            else //Si el usuario no está logeado. Abrimos la página de login. El usuario tiene que acceder a su cuenta para poder comprar un producto.
            {

                return RedirectToAction("Login", "Account", new { returnUrl = "Login" });
            }
        }

    }
}
