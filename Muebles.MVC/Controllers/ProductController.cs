﻿using Muebles.Application;
using Muebles.CORE;
using Muebles.IFR;
using Muebles.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión de productos
    /// </summary>
    public class ProductController : Controller
    {

        readonly IProductManager productManager;
        readonly IPictureManager pictureManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempProduct">manager de productos</param>
        /// <param name="tempPicture">manager de imagenes</param>
        public ProductController(IProductManager tempProduct, PictureManager tempPicture)
        {
            this.productManager = tempProduct;
            this.pictureManager = tempPicture;
        }

        public ProductController() { }

        /// <summary>
        /// Método para mostrar todos los productos
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Index()
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();

            try
            {
                List<Product> lista = productManager.GetAll().ToList();
                if (lista.Count != 0)
                {
                    //Creamos un modelo de vista con los datos de los productos
                    var productos = lista.Select(i => new ProductViewModel()
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Description = i.Description,
                        Price = i.Price,
                        Stock = i.Stock,
                        Category = i.Category
                    });
                    return View(productos); //devolvemos el modelo de vista
                }
                else
                {
                    return View();
                }

            }
            catch (NullReferenceException ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para mostrar los detalles del producto que se desea modificar
        /// <param name="id">Id del producto a modificar</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Details(int id)
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();

            try
            {
                Product i = productManager.GetById(id); //obtenemos el producto con el id recibido

                //si el producto no es nulo creamos un modelo de vista con los datos del pedido
                if (i != null)
                {
                    ProductViewModel vm = new ProductViewModel()
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Description = i.Description,
                        Price = i.Price,
                        Stock = i.Stock,
                        Category = i.Category
                    };
                    return View(vm); //devolvemos el modelo de vista
                }
                else
                {
                    //si el producto no existe, abrimos la página creación de un nuevo producto
                    return RedirectToAction("Create");
                }
            }
            catch (NullReferenceException ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para mostrar el formulario de creación del producto
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Método para guardar el nuevo producto
        /// <param name="i">Modelo de la vista del producto</param>
        /// <param name="Image1">Imagen 1 del producto</param>
        /// <param name="Image2">Imagen 2 del producto</param>
        /// <param name="Image3">Imagen 3 del producto</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Create(ProductViewModel i, HttpPostedFileBase image1, HttpPostedFileBase image2, HttpPostedFileBase image3)
        {
            try
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();

                //Creamos el producto
                Product pro = new Product()
                {
                    Id = i.Id,
                    Name = i.Name,
                    Description = i.Description,
                    Price = i.Price,
                    Stock = i.Stock,
                    Category = i.Category
                };

                //añadimos el producto al contexto
                productManager.Add(pro);
                context.SaveChanges();

                //subimos las imagenes y guardamos las rutas en el contexto
                if (image1 != null && image1.ContentLength > 0)
                {
                    saveImage(pro, image1);
                }
                if (image2 != null && image2.ContentLength > 0)
                {
                    saveImage(pro, image2);
                }
                if (image3 != null && image3.ContentLength > 0)
                {
                    saveImage(pro, image3);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }
        /// <summary>
        /// Método que sube imágenes de productos al servidor y guarda su ruta en la base de datos
        /// <param name="product">Producto al que se le va a asociar una imagen</param>
        /// <param name="image">Imagen a subir</param>
        /// </summary>
        public void saveImage(Product product, HttpPostedFileBase image)
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            Picture picture = null;

            try
            {
                //guardaremos  la imagen en la carpeta images
                string path = Path.Combine(Server.MapPath("../images/"), Path.GetFileName(image.FileName));
                image.SaveAs(path);

                //Instanciamos una nueva imagen
                picture = new Picture()
                {
                    Route = Path.GetFileName(image.FileName),
                    ProductId = product.Id
                };
                //Añadimos la imagen al contexto
                pictureManager.Add(picture);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
            }
        }


        /// <summary>
        /// Método para mostrar los detalles del producto que se desea modificar
        /// <param name="id">Id del producto a modificar</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Edit(int id)
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
            try
            {
                Product i = productManager.GetById(id); //Obtenemos el producto con el id pasado por parémetro
                if (i != null)
                {
                    //Creamos un modelo de vista con los datos del producto
                    ProductViewModel vm = new ProductViewModel()
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Description = i.Description,
                        Price = i.Price,
                        Stock = i.Stock,
                        Category = i.Category,
                        Image1 = pictureManager.GetRoute(i.Id, 1),
                        Image2 = pictureManager.GetRoute(i.Id, 2),
                        Image3 = pictureManager.GetRoute(i.Id, 3),
                    };
                    return View(vm); //devolvemos el modelo a la vista
                }
                else
                {
                    return RedirectToAction("Create"); //si el producto no existe, abrimos la página creación de un nuevo producto
                }
            }
            catch (NullReferenceException ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para guardar los cambios en los datos del producto
        /// <param name="id">Id del producto</param>
        /// <param name="i">Modelo de la vista de datos del producto</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Edit(int id, ProductViewModel i)
        {
            try
            {
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
                Product p = productManager.GetById(id); //Obtenemos el producto con el id pasado por parémetro

                //Actualizamos los datos del producto
                p.Id = i.Id;
                p.Name = i.Name;
                p.Description = i.Description;
                p.Price = i.Price;
                p.Stock = i.Stock;
                p.Category = i.Category;
                context.SaveChanges();

                return RedirectToAction("Index"); //abrimos la página de la lista de productos

            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }


        }

        /// <summary>
        /// Método para mostrar los datos del producto que se desea eliminar
        /// <param name="id">Id del producto a eliminar</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Delete(int id)
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            try
            {
                Product i = productManager.GetById(id); //Obtenemos el producto con el id pasado por parémetro
                if (i != null)
                { //Si no es nulo creamos un modelo de vista con los datos del producto
                    ProductViewModel vm = new ProductViewModel()
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Description = i.Description,
                        Price = i.Price,
                        Stock = i.Stock,
                        Category = i.Category
                    };
                    return View(vm); //devolvemos el modelo de vista
                }
                else
                {
                    return RedirectToAction("Create"); //si el producto no existe, abrimos la página creación de un nuevo producto
                }
            }
            catch (NullReferenceException ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para eliminar el producto con el id pasado por parámetro
        /// <param name="collection">Id del producto a eliminar</param>
        /// <param name="i">Modelo de la vista de datos del producto</param>
        /// <returns>Resultado de la acción</returns>
        [HttpPost]
        public ActionResult Delete(int id, ProductViewModel collection)
        {
            try
            {
                // TODO: Add delete logic here
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();

                productManager.RemoveById(id); //elimiamos el producto 
                context.SaveChanges(); //guardamos los cambios

                return RedirectToAction("Index");//abrimos la página de la lista de productos
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para eliminar una imagen del producto
        /// <param name="id">Id del productor</param>
        /// <param name="ruta">Ruta de la imagen</param>
        /// <param name="number">Número d ela imagen</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult DeleteImage(int id, string route, int number)
        {
            try
            {

                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
                Product p = productManager.GetById(id); //Obtenemos el producto cuya imagen queremos eliminar

                //eliminamos la imagen con la ruta pasada por parámetro
                pictureManager.RemoveByProductId(id, number);
                context.SaveChanges();
                return RedirectToAction("Edit", new { id = id }); //actualizamos la página de edición del producto
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return RedirectToAction("Edit", new { id = id });
            }
        }
    }
}
