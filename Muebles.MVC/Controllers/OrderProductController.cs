﻿using Muebles.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using Muebles.CORE;
using System.Web.Mvc;
using Muebles.MVC.Models;
using Muebles.IFR;

namespace Muebles.MVC.Controllers
{
    /// <summary>
    /// Clase de gestión de líneas de pedidos
    /// </summary>
    public class OrderProductController : Controller
    {

        readonly IOrderProductManager orderProductManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tempOrderProduct">manager</param>
        public OrderProductController(IOrderProductManager tempOrderProduct)
        {
            this.orderProductManager = tempOrderProduct;
        }

        public OrderProductController() { }

        /// <summary>
        /// Método para mostrar todas las líneas del pedido cuyo id coincide con el pasado por parámetro
        /// <param name="id">Id del pedido</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Index(int idPedido)
        {

            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            ProductManager productManager = new ProductManager(context);

            try
            {
                List<OrderProduct> lista = orderProductManager.GetOPByOrderId(idPedido).ToList(); //Obtenemos la lista de las líneas del pedido con el id pasado por parémtro
                if (lista.Count != 0)
                {
                    //Creamos un modelo de vista con los datos de las líneas del pedido
                    var lineas = lista.Select(i => new OrderProductViewModel()
                    {
                        Id = i.Id,
                        ProductId = i.ProductId,
                        ProductName = productManager.GetById(i.ProductId).Name, //Nombre del producto
                        ProductPrice = productManager.GetById(i.ProductId).Price, //Precio unitario del producto
                        Number = i.Number,
                        Price = productManager.GetById(i.ProductId).Price * i.Number //Precio
                    });
                    return View(lineas); //devolvemos el modelo de vista
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para mostrar el formulario de creación de línea de pedido
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Método para guardar la nueva línea de pedido
        /// <param name="i">Modelo de la vista de la línea de pedido</param>
        /// <param name="id">Id del pedido</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Create(OrderProductViewModel i, int idPedido)
        {

            float total = (float)0.0; //total del pedido
            float price = (float)0.0; //precio de la línea de pedido

            try
            {
                
                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
              
                //Creamos una nueva línea de pedido
                OrderProduct or = new OrderProduct()
                {
                    Id = i.Id,
                    OrderId = idPedido,
                    ProductId = i.ProductId,
                    Number = i.Number

                };
                //Añadimos la línea de pedido al contexto de datos
                orderProductManager.Add(or);
                context.SaveChanges();

                OrderManager orderManager = new OrderManager(context);
                Order pedido = orderManager.GetById(idPedido); //Obtenemos el pedido con el id pasado por parámetro

                ProductManager productManager = new ProductManager(context);
                Product producto = new Product();

                List<OrderProduct> listaOP = new List<OrderProduct>();
                listaOP = orderProductManager.GetOPByOrderId(pedido.Id).ToList(); //obtenemos la lista de las líneas del pedido

                //Recorremos la lista y actualizamos el total del pedido
                foreach (OrderProduct op in listaOP)
                {
                    producto = productManager.GetById(i.ProductId);
                    price = producto.Price * op.Number;
                    total += price;  
                }

                pedido.Total = total; //asignamos el total a pagar al pedido
                context.SaveChanges(); //Guardamos los cambios

                return RedirectToAction("Index", new { idPedido = pedido.Id }); //Abrimos la página de la lista de líneas de pedido
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para mostrar los detalles de la línea de pedido que se desea modificar
        /// <param name="id">Id de la línea de pedido a modificar</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        public ActionResult Edit(int id)
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
            
            try
            {
                ProductManager productManager = new ProductManager(context);
                OrderProduct i = orderProductManager.GetById(id); //Obtenemos la línea de pedido con el id pasado por parámetro
                if (i != null)
                {
                    //Creamos un modelo de vista con los datos de la línea del pedido
                    OrderProductViewModel vm = new OrderProductViewModel()
                    {
                        ProductId = i.ProductId,
                        ProductName = productManager.GetById(i.ProductId).Name,
                        Number = i.Number,

                    };
                    return View(vm); //devolvemos el modelo de vista
                }
                else
                {
                    //Si la línea de pedido no existe, abrimos la página de creación de una nueva línea de pedido
                    return RedirectToAction("Create");
                }
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        /// <summary>
        /// Método para guardar los cambios en los datos de la línea de pedido
        /// <param name="id">Id de la línea de pedido</param>
        /// <param name="i">Modelo de la vista de datos de la línea de pedido</param>
        /// <returns>Resultado de la acción</returns>
        /// </summary>
        [HttpPost]
        public ActionResult Edit(int id, OrderProductViewModel i)
        {

            #region Inicialización de las variables
            float total = (float)0.0;
            float price = (float)0.0;
            Product producto = new Product();
            List<OrderProduct> listaOP = new List<OrderProduct>();
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            #endregion

            try
            {
                
                ProductManager productManager = new ProductManager(context); //manager de productos

                OrderProduct p = orderProductManager.GetById(id); //obtenemos la línea de pedido con el id pasado por parámetro
                //actualizamos sus valores
                p.Id = i.Id;
                p.ProductId = i.ProductId;
                p.Number = i.Number;

                OrderManager orderManager = new OrderManager(context);
                Order pedido = orderManager.GetById(p.OrderId);  //Obtenemos el pedido al que pertenece la línea de pedido


                listaOP = orderProductManager.GetOPByOrderId(pedido.Id).ToList(); //obtenemos la lista de las líneas del pedido
                if (listaOP.Count != 0)
                {
                    //si no está vacía, la recorremos para actualizr el total del pedido
                    foreach (OrderProduct op in listaOP) 
                    {
                        producto = productManager.GetById(i.ProductId);
                        price = producto.Price * op.Number;
                        total += price; //calculamos el total del pedido
                    }
                }

                //asignamos suma total a pagar al pedido y guardamos los cambios
                pedido.Total = total;
                context.SaveChanges();

                return RedirectToAction("Index", new { idPedido = pedido.Id }); //Abrimos la página de la lista de líneas de pedido
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }


        /// <summary>
        /// Método que elimina una línea de pedido del pedido
        /// </summary>
        /// <param name="id">Id de la línea de pedido</param>
        /// <returns>Resultado de la acción</returns>
        public ActionResult DeleteItem(int id)
        {
            try
            {

                DAL.ApplicationDbContext context = new DAL.ApplicationDbContext(); //contexto de datos
                OrderProduct item = orderProductManager.GetById(id); //Obtenemos la llínea de pedido a partir de su id

                OrderManager orderManager = new OrderManager(context);
                Order pedido = orderManager.GetById(item.OrderId); //obtenemos el pedido al que pertenece la línea de padido a partir de su id

                ProductManager productManager = new ProductManager(context);
                Product producto = productManager.GetById(item.ProductId); //obtenemos el producto de la línea de padido a partir de su id

                pedido.Total -= (item.Number * producto.Price); //disminuimos el total del pedido

                orderProductManager.RemoveById(id); //eliminamos la línea de pedido
                context.SaveChanges(); //guardamos los cambios
                TempData["Success"] = "El item sa ha eliminado con éxito.";

                return RedirectToAction("Index", new { idPedido = pedido.Id }); //Abrimos la página de la lista de líneas de pedido
            }
            catch (Exception ex)
            {
                //Guardamos información de la excepción en el archivo de log
                Log.LogError(ex.Message, ex);
                return View();
            }
        }


    }
}
