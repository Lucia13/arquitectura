﻿
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Muebles.MVC.Models
{
    // Puede agregar datos del perfil del usuario agregando más propiedades a la clase ApplicationUser. Para más información, visite http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        
        public string Name { get; internal set; }
        public string Surnames { get; internal set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        
        public System.Data.Entity.DbSet<Muebles.MVC.Models.ProductViewModel> ProductViewModels { get; set; }
        
        public System.Data.Entity.DbSet<Muebles.MVC.Models.OrderViewModel> OrderViewModels { get; set; }

        public System.Data.Entity.DbSet<Muebles.MVC.Models.OrderProductViewModel> OrderProductViewModels { get; set; }

        public System.Data.Entity.DbSet<Muebles.MVC.Models.UsersViewModel> UsersViewModels { get; set; }

        public System.Data.Entity.DbSet<Muebles.MVC.Models.UserOrderViewModel> UserOrderViewModels { get; set; }

        
        public System.Data.Entity.DbSet<Muebles.MVC.Models.AddressViewModel> AddressViewModels { get; set; }

        
        public System.Data.Entity.DbSet<Muebles.MVC.Models.CartViewModel> CartViewModels { get; set; }

        public System.Data.Entity.DbSet<Muebles.MVC.Models.CatalogViewModel> CatalogViewModels { get; set; }

        public System.Data.Entity.DbSet<Muebles.MVC.Models.PayViewModel> PayViewModels { get; set; }
    }
}