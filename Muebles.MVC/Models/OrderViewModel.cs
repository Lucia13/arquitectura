﻿using Muebles.CORE;
using System;

namespace Muebles.MVC.Models
{
    public partial class OrderViewModel
    {
        /// <summary>
        /// Identificador del pedido
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Fecha de pedido
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// Nombre de usuario del cliente
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Total  a pagar
        /// </summary>
        public float Total { get; set; }

      
    }
}
