﻿

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase modelo de líneas de pedido en el carrito
    /// </summary
    public partial class CartViewModel
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///  Id del producto
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        ///  Nombre del producto
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        ///  Precio unitario"
        public float ProductPrice { get; set; }

        /// <summary>
        /// Cantidad
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Precio
        /// </summary>
        public float Price { get; set; }


    }
}