﻿using Muebles.CORE;
using System.ComponentModel.DataAnnotations;

namespace Muebles.MVC.Models
{

    /// <summary>
    /// Clase modelo de productos del catálogo
    /// </summary
    public partial class CatalogViewModel
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// Nombre del producto
        /// </summary>

        public string Name { get; set; }

        /// <summary>
        /// Descripción del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public float Price { get; set; }


        /// <summary>
        /// Imagen del producto
        /// </summary>
        public string Image1 { get; set; }


        /// <summary>
        /// Imagen del producto
        public string Image2 { get; set; }

        /// <summary>
        /// Imagen del producto
        /// </summary>
        public string Image3 { get; set; }

        /// <summary>
        /// Categoria del producto
        /// </summary>
        public Category Category { get; set; }

    }
}