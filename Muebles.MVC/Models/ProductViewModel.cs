﻿using Muebles.CORE;
using System.ComponentModel.DataAnnotations;

namespace Muebles.MVC.Models
{

    /// <summary>
    /// Clase modelo de productos
    /// </summary
    public partial class ProductViewModel
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripción del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// Stock del producto
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// Categoría del producto
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// Imagen del producto
        public string Image1 { get; set; }

        /// <summary>
        /// Imagen del producto
        public string Image2 { get; set; }

        /// <summary>
        /// Imagen del producto
        /// </summary>
        public string Image3 { get; set; }






    }
}