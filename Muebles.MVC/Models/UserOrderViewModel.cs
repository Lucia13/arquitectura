﻿using Muebles.CORE;
using System;

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase modelo de líneas de pedido
    /// </summary
    public partial class UserOrderViewModel
    {
        /// <summary>
        /// Identificador del pedido
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Fecha de pedido
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Total  a pagar
        /// </summary>
        public float Total { get; set; }

    }
}

