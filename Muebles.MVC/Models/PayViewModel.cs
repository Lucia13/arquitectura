﻿
namespace Muebles.MVC.Models
{

    /// <summary>
    /// Clase modelo de pago
    /// </summary
    public partial class PayViewModel
    {
        /// <summary>
        /// Identificador del pago
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Titular de la tarjeta
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Número de la tarjeta
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Código CCV
        /// </summary>
        public int CCV { get; set; }

        /// <summary>
        /// Caducidad
        /// </summary>
        public string Expiration { get; set; }
    }
}
