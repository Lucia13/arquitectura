﻿
using Muebles.CORE;


namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase modelo de usuarios
    /// </summary
    public partial class UsersViewModel
    {

        public string Id { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>

        public string Name { get; set; }

        /// <summary>
        /// Apellidos
        /// </summary>
        public string Surnames { get; set; }

        /// <summary>
        /// Dirección
        /// </summary>
        public string Street { get; set; }


        /// <summary>
        /// Ciudad
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Provincia
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Código postal
        /// </summary>
        public string CP { get; set; }

        /// <summary>
        /// País
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Teléfono
        /// </summary>
        public string Phone { get; set; }


    }
}