﻿using Muebles.CORE;
using System.ComponentModel.DataAnnotations;

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase modelo de líneas de pedido
    /// </summary
    public partial class OrderProductViewModel
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id del producto
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        ///  Nombre del producto
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        ///  Precio unitario"
        /// </summary>
        public float ProductPrice { get; set; }

        /// <summary>
        /// Cantidad
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Precio
        /// </summary>
        public float Price { get; set; }

    }
}