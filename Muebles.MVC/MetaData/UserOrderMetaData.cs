﻿using Muebles.CORE;
using System;
using System.ComponentModel.DataAnnotations;

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase de anotaciones de la clase UserOrderViewModel
    /// </summary
    public class UserOrderMetaData
    {

        /// <summary>
        /// Fecha de pedido
        /// </summary>
        [Display(Name = "Fecha")]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>

        [Display(Name = "Estado")]
        public Status Status { get; set; }
    }


    [MetadataType(typeof(UserOrderMetaData))]
    public partial class UserOrderViewModel { }
}

