﻿using Muebles.CORE;
using System.ComponentModel.DataAnnotations;

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase de anotaciones de la clase CatalogViewModel
    /// </summary
    public class CatalogMetaData
    {

        /// <summary>
        /// Nombre del producto
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Descripción del producto
        /// </summary>
        [Required]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        [Required]
        [Display(Name = "Precio")]
        public float Price { get; set; }


    }


    [MetadataType(typeof(CatalogMetaData))]
    public partial class CatalogViewModel { }
}