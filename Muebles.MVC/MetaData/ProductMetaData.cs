﻿using Muebles.CORE;
using Muebles.MVC.Models;
using System.ComponentModel.DataAnnotations;

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase de anotaciones de la clase ProductViewModel
    /// </summary
    public class ProductMetaData
    {

        /// <summary>
        /// Nombre del producto
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Descripción del producto
        /// </summary>
        [Required]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        [Required]
        [Range(0, 300000)]
        [Display(Name = "Precio")]
        public float Price { get; set; }

        /// <summary>
        /// Stock del producto
        /// </summary>
        [Required]
        [Range(0, 300000)]
        public int Stock { get; set; }

        /// <summary>
        /// Categoría del producto
        /// </summary>
        [Required]
        [Display(Name = "Categoría")]
        public Category Category { get; set; }


        /// <summary>
        /// Imagen del producto
        /// </summary>
        [Display(Name = "Imagen 1")]
        public string Image1 { get; set; }


        /// <summary>
        /// Imagen del producto
        [Display(Name = "Imagen 2")]
        public string Image2 { get; set; }

        /// <summary>
        /// Imagen del producto
        /// </summary>
        [Display(Name = "Imagen 3")]
        public string Image3 { get; set; }
    }

    [MetadataType(typeof(ProductMetaData))]
    public partial class ProductViewModel { }

}
