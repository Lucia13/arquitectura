﻿using Muebles.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase de anotaciones de la clase OrderViewModel
    /// </summary
    public class OrderMetaData
    {

        /// <summary>
        /// Fecha de pedido
        /// </summary>
        [Display(Name = "Fecha")]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        [Display(Name = "Id del cliente")]
        public string Client { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        [Display(Name = "Nombre de cliente")]
        public string ClientName { get; set; }

        /// <summary>
        /// Nombre de usuario del cliente
        /// </summary>
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>

        [Display(Name = "Estado")]
        public Status Status { get; set; }
    }


    [MetadataType(typeof(OrderMetaData))]
    public partial class OrderViewModel { }
}
