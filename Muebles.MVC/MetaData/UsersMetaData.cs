﻿

using System.ComponentModel.DataAnnotations;
using Muebles.CORE;


namespace Muebles.MVC.Models
{

    /// <summary>
    /// Clase de anotaciones de la clase UsersViewModel
    /// </summary
    public class UsersMetaData
    {
        /// <summary>
        /// Nombre
        /// </summary>
        [Required, StringLength(100)]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Apellidos
        /// </summary>
        [Required]
        [Display(Name = "Apellidos")]
        public string Surnames { get; set; }

        /// <summary>
        /// Dirección
        /// </summary>
        [Required]
        [Display(Name = "Dirección")]
        public string Street { get; set; }


        /// <summary>
        /// Ciudad
        /// </summary>
        [Required]
        [Display(Name = "Ciudad")]
        public string City { get; set; }

        /// <summary>
        /// Provincia
        /// </summary>
        [Required]
        [Display(Name = "Provincia")]
        public string Province { get; set; }

        /// <summary>
        /// Código postal
        /// </summary>
        [Required]
        [Display(Name = "Código postal")]
        public string CP { get; set; }

        /// <summary>
        /// País
        /// </summary>

        [Display(Name = "País")]
        public Country Country { get; set; }

        [Required, StringLength(100)]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        /// <summary>
        /// Teléfono
        /// </summary>
        [Phone]
        [Display(Name = "Teléfono")]
        public string Phone { get; set; }
    }

    [MetadataType(typeof(UsersMetaData))]
    public partial class UsersViewModel { }

}