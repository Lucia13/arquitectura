﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase de anotaciones de la clase CartViewModel
    /// </summary
    public class CartMetaData
    {
       
        /// <summary>
        ///  Id del producto
        /// </summary>
        [Required]
        [Display(Name = "Id de producto")]
        public int ProductId { get; set; }

        /// <summary>
        ///  Nombre del producto
        /// </summary>
        [Required]
        [Display(Name = "Nombre de producto")]
        public string ProductName { get; set; }

        /// <summary>
        ///  Precio unitario"
        /// </summary>
        [Display(Name = "Precio unitario")]
        public float ProductPrice { get; set; }

        /// <summary>
        /// Cantidad
        /// </summary>
        [Required]
        [Display(Name = "Cantidad")]
        public int Number { get; set; }

        /// <summary>
        /// Precio
        /// </summary>
        [Display(Name = "Precio")]
        public float Price { get; set; }


    }

    [MetadataType(typeof(CartMetaData))]
    public partial class CartViewModel { }
}