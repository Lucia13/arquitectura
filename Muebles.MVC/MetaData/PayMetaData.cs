﻿using Muebles.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Muebles.MVC.Models
{
    /// <summary>
    /// Clase de anotaciones de la clase PayViewModel
    /// </summary
    public class PayMetaData
    {
        /// <summary>
        /// Titular de la tarjeta
        /// </summary>
        [Required]
        [Display(Name = "Titular")]
        public string Owner { get; set; }

        /// <summary>
        /// Número de la tarjeta
        /// </summary>
        [Required]
        [Display(Name = "Número")]
        public string Number { get; set; }

        /// <summary>
        /// Código CCV
        /// </summary>
        [Required]
        [Display(Name = "CCV")]
        public int CCV { get; set; }

        /// <summary>
        /// Caducidad
        /// </summary>
        [Required]
        [Display(Name = "Caducidad")]
        public string Expiration { get; set; }
    }


    [MetadataType(typeof(PayMetaData))]
    public partial class PayViewModel { }
}
