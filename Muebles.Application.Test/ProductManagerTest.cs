﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Muebles.DAL;
using Muebles.CORE;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;

namespace Muebles.Application.Test
{
    ///<summary>
    /// Prueba de los métodos de la clase ProductManager
    /// </summary>
    [TestClass]
    public class ProductManagerTest
    {

        /*
        ///<summary>
        /// Prueba de si el método GetAll busca productos correctamente
        /// </summary>
        [TestMethod]
        public void GetAll()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            var products = manager.GetAll();

            //Assert
            Assert.IsNotNull(products);
        }

        ///<summary>
        /// Prueba de si el método GetById busca productos por id correctamente
        /// </summary>
        [TestMethod]
        public void GetByIdNull()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            Product product = manager.GetById(-2);

            //Assert
            Assert.IsNull(product);
        }

        ///<summary>
        /// Prueba de si el método GetById devuelve el producto con el id pasado por parámetro
        /// </summary>
        [TestMethod]
        public void GetByIdGood()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            Product product = manager.GetById(1017);

            //Assert
            if (product != null)
            {
                Assert.AreEqual(product.Id, 1017);
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría dormitorio
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryDormitoryGood1()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("dormitorio").ToList();
            if (list.Count()!=0)
            {
                foreach (Product product in list)
                {
                    //Assert
                    Assert.IsTrue(product.Category == Category.Dormitorio);
                }
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría dormitorio
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryDormitoryGood2()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("dormitorio").ToList();
            if (list.Count() != 0)
            {
                foreach (Product product in list)
                {
                    //Assert     
                    Assert.IsFalse(product.Category == Category.Cocina);
                }
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría dormitorio
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryDormitoryGood3()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("dormitorio").ToList();
            if (list.Count() != 0)
            {
                foreach (Product product in list)
                {
                    //Assert
                    Assert.IsFalse(product.Category == Category.Salón);
                }
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría salón
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryLivingRoomGood1()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("salon").ToList();
            if (list.Count() != 0)
            {
                foreach (Product product in list)
                {
                    //Assert
                    Assert.IsTrue(product.Category == Category.Salón);
                }
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría salón
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryLivingRoomGood2()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("salon").ToList();
            if (list.Count() != 0)
            {
                foreach (Product product in list)
                {
                    //Assert
                    Assert.IsFalse(product.Category == Category.Cocina);
                }
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría salón
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryLivingRoomGood3()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("salon").ToList();
            if (list.Count() != 0)
            {
                foreach (Product product in list)
                {
                    //Assert
                    Assert.IsFalse(product.Category == Category.Dormitorio);
                }
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría cocina
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryKitchenGood1()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("cocina").ToList();
            if (list.Count() != 0)
            {
                foreach (Product product in list)
                {
                    //Assert
                    Assert.IsTrue(product.Category == Category.Cocina);
                }
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría cocina
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryKitchenGood2()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("cocina").ToList();
            if (list.Count() != 0)
            {
                foreach (Product product in list)
                {
                    //Assert              
                    Assert.IsFalse(product.Category == Category.Salón);
                }
            }
        }

        ///<summary>
        /// Prueba de si el método GetAllByCategory devuelve solo productos de la categoría cocina
        /// </summary>
        [TestMethod]
        public void GetAllByCategoryKitchenGood3()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            //Act
            List<Product> list = new List<Product>();
            list = manager.GetAllByCategory("cocina").ToList();
            if (list.Count() != 0)
            {
                foreach (Product product in list)
                {
                    //Assert
                    Assert.IsFalse(product.Category == Category.Dormitorio);
                }
            }
        }
        

        ///<summary>
        /// Prueba de si el método ToStringCategory convierte una categoría en un string
        /// </summary>
        [TestMethod]
        public void ToStringCategoryNotNullTest()
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            ProductManager manager = new ProductManager(context);
            string categoria = manager.ToStringCategory(Category.Salón);

            //Assert
            Assert.IsNotNull(categoria);
        }


        ///<summary>
        /// Prueba de si el método ToStringCategory convierte una categoría en un string correcto
        /// </summary>
        [TestMethod]
        public void ToStringCategoryTest()
        {
            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            ProductManager manager = new ProductManager(context);
            string categoria = manager.ToStringCategory(Category.Dormitorio);

            //Assert
            Assert.AreEqual("dormitorio", categoria);
        }*/


    }
}
