﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Muebles.DAL;
using Muebles.CORE;
using System;
using System.IO;

namespace Muebles.Application.Test
{
    ///<summary>
    /// Clase que prueba los métodos de la clase OrderManager
    /// </summary>
    [TestClass]
    public class OrderManagerTest
    {/*

        [TestInitialize]
        public void TestInitialize()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + @"\..\..\..\Muebles.Web\App_Data"));
        }

        ///<summary>
        /// Prueba de si el método GetAll busca pedidos correctamente
        /// </summary>
        [TestMethod]
        public void GetAll()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager manager = new OrderManager(context);

            //Act
            var orders = manager.GetAll();

            //Assert
            Assert.IsNotNull(orders);
        }


        ///<summary>
        /// Prueba de si el método GetById busca pedidos por id correctamente
        /// </summary>
        [TestMethod]
        public void GetByIdNull()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager manager = new OrderManager(context);

            //Act
            Order order = manager.GetById(-3);

            //Assert
            Assert.IsNull(order);
        }

        ///<summary>
        /// Prueba de si el método GetById devuelve el pedido con el id pasado por parámetro
        /// </summary>
        [TestMethod]
        public void GetByIdGood()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager manager = new OrderManager(context);

            //Act
            Order order = manager.GetById(1036);

            //Assert
            if (order != null)
            {
                Assert.AreEqual(order.Id, 1036);
            }
        }

        ///<summary>
        /// Prueba de si el método GetByUserIdTodayPending devuelve pedido nulo para un usuario no existente
        /// </summary>
        [TestMethod]
        public void GetByUserIdTodayPendingNull()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager manager = new OrderManager(context);

            //Act
            Order order = manager.GetByUserIdTodayPending("3");

            //Assert
            Assert.IsNull(order);
        }

        ///<summary>
        /// Prueba de si el método GetByUserIdTodayPending devuelve pedidos pagados en lugar de pendientes de usuarios con ids pasados por parámetro 
        /// </summary>
        [TestMethod]
        public void GetByUserIdTodayPendingPagado()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager manager = new OrderManager(context);

            //Act
            Order order = manager.GetByUserIdTodayPending("39b74677-f1e2-45e4-b93d-85fd7098bc05");

            //Assert
            if (order != null)
            {
                Assert.IsFalse(order.Status == Status.Pagado);
            }
        }

        */
    }
}
