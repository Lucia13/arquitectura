﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Muebles.DAL;
using Muebles.CORE;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;

namespace Muebles.Application.Test
{
    /// <summary>
    /// Clase que testea los métodos de la clase OrderProdct
    /// </summary>
    [TestClass]
    public class ProductOrderManagerTest
    {
        /*
        [TestInitialize]
        public void TestInitialize()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + @"\..\..\..\Muebles.Web\App_Data"));
        }

        ///<summary>
        /// Prueba de si el método GetAll busca líneas de pedido correctamente
        /// </summary>
        [TestMethod]
        public void GetAll()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            OrderProductManager manager = new OrderProductManager(context);

            //Act
            var products = manager.GetAll();

            //Assert
            Assert.IsNotNull(products);
        }


        ///<summary>
        /// Prueba de si el método GetOPByOrderId devuelve los productos del pedido con el id pasado por parámetro
        /// </summary>
        [TestMethod]
        public void GetOpByOrderIdGood()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            OrderProductManager manager = new OrderProductManager(context);

            //Act
            List<OrderProduct> list = new List<OrderProduct>();
            list = manager.GetOPByOrderId(1036).ToList();
            if (list.Count() != 0)
            {
                foreach (OrderProduct product in list)
                {
                    //Assert
                    Assert.IsTrue(product.OrderId == 1036);
                }
            }
        }
        */
    }
}
