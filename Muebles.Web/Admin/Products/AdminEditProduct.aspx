﻿<%@ Page Title="Editar producto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminEditProduct.aspx.cs" Inherits="Muebles.Web.Admin.Products.AdminEditProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Productos</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

     <div class="form-horizontal">
        <h4 style="color: #591d1d">Modificar o borrar producto</h4>
        <hr />

         <asp:ValidationSummary  ID="ValidationSummary1" runat="server" CssClass="text-danger" />    
        
        <div class="form-group">
            <asp:Label ID="lblText" runat="server" Text="Nombre:"  CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtNombre" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="lblResolucion" runat="server" Text="Descripción:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtDescripcion" runat="server" Width="300px" CssClass="form-control" TabIndex="1" ></asp:TextBox>
            </div>
        </div>    

         <div class="form-group">
            <asp:Label ID="lblNota" runat="server" Text="Precio:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtPrecio" runat="server" Width="200px" CssClass="form-control" TabIndex="2" ></asp:TextBox>
            </div>
        </div>   

         <div class="form-group">
            <asp:Label ID="lblStock" runat="server" Text="Stock:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtStock" runat="server" Width="200px" CssClass="form-control" TabIndex="3" TextMode="Number" ></asp:TextBox>
            </div>
        </div>   
         
         
         <div class="form-group">
            <asp:Label ID="lblCategoria" runat="server" Text="Categoría:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:DropDownList ID="ddlCategoria" runat="server"  CssClass="form-control" Width="200px" TabIndex="4">       
                    </asp:DropDownList>  
            </div>
        </div>  
         
 
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="cmdSave" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" runat="server" Text="Modificar" OnClick="cmdSave_Click" CssClass="btn btn-default" />
                <asp:Button ID="cmdDelete" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" runat="server" Text="Borrar" OnClick="cmdDelete_Click" CssClass="btn btn-default" TabIndex="6" />
                <asp:Label ID="lblResult" runat="server" ForeColor="#33CC33"></asp:Label>
            </div>
        </div>



      </div>

  <script>
        $(document).ready(function(){
        $("#cmdDelete").click(function(){
        alert('¡El producto se eliminará!');
        });
        });
  </script>
</asp:Content>

