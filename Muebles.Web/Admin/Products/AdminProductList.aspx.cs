﻿using Muebles.Application;
using Muebles.CORE;
using Muebles.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;

namespace Muebles.Web.Admin.Products
{
    /// <summary>
    /// Clase que muestra listados de productos disponibles y agotados
    /// <summary>
    public partial class AdminProductList : System.Web.UI.Page
    {

        //Método de carga de la página
        protected void Page_Load(object sender, EventArgs e)
        {

            //establecemos cabeceras de las tablas
            //productos disponibles
            GridView1.UseAccessibleHeader = true;
            if (GridView1.HeaderRow != null)
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;

            //productos agotados
            GridView2.UseAccessibleHeader = true;
            if (GridView2.HeaderRow != null)
                GridView2.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        /// <summary>
        /// Metodo que retorna los datos de los productos disponibles para el listado 
        /// </summary>
        /// <returns>Lista de productos disponibles </returns>
        public List<Product> GetDataAvailable()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager productManager = new ProductManager(context);

            //lista de productos disponibles 
            List<Product> list = new List<Product>();
            try { 
            list = productManager.GetAllWithStock().ToList();
            }

            //guardamos excepciones en la base de datos
            catch (Exception ex)
            {
                
            }
            return list;
        }

        /// <summary>
        /// Metodo que retorna los datos de los productos agotados para el listado 
        /// </summary>
        /// <returns>Lista de productos agotados </returns>
        public List<Product> GetDataNotAvailable()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager productManager = new ProductManager(context);

            //lista de productos agotados 
            List<Product> list = new List<Product>();
            try { 
                list = productManager.GetAllWithoutStock().ToList();
            }

            //guardamos excepciones en la base de datos
            catch (Exception ex)
            {
            
            }

            return list;
        }


        /// <summary>
        /// Método - Al hacer click sobre Nuevo producto se abre nuevo formulario
        /// </summary>
        protected void cmdNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminCreateProduct");
        }
    }
}