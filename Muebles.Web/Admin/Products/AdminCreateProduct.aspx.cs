﻿using Muebles.Application;
using Muebles.CORE;
using Muebles.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Muebles.Web.Admin.Products
{
    /// <summary>
    /// Clase de inserción de productos nuevos por el administrador
    /// </summary>
    public partial class AdminCreateProduct : System.Web.UI.Page
    {
        /// <summary>
        ///Método de carga de la página
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {

            //llenamos la caja desplegable de categorías
            if (!Page.IsPostBack)
            {
                ddlCategoria.DataSource = null;
                Dictionary<string, string> valuesEnum = new Dictionary<string, string>();
                var values = Enum.GetValues(typeof(Category));
                foreach (var value in values)
                {
                    valuesEnum.Add(((int)value).ToString(), value.ToString());
                }
                ddlCategoria.DataTextField = "Value";
                ddlCategoria.DataValueField = "Key";
                ddlCategoria.DataSource = valuesEnum;
                ddlCategoria.DataBind();
            }
        }

        /// <summary>
        ///Método que crea un producto y lo guarda a la base de datos
        /// </summary>
        protected void cmdSave_Click(object sender, EventArgs e)
        {

            //variables
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager productManager = new ProductManager(context);   

            //creamos un nuevo producto
            try
            {
                Product product = new Product()
                {
                    Name = txtNombre.Text,
                    Description = txtDescripcion.Text,
                    Price = float.Parse(txtPrecio.Text),
                    Stock = Convert.ToInt32(txtStock.Text),
                    Category = (Category)Enum.Parse(typeof(Category), ddlCategoria.SelectedValue),
                };

                //guardamos el producto a la base de datos
                productManager.Add(product);
                context.SaveChanges();

                //Subida de la imagen 1
                if (FileUpload1.HasFile)
                {
                    subirImagen(product, FileUpload1);
                    lblSubida1.Text = "La imagen sa ha cargado con éxito.";
                }

                //Subida de la imagen 2
                if (FileUpload2.HasFile)
                {
                    subirImagen(product, FileUpload2);
                    lblSubida2.Text = "La imagen sa ha cargado con éxito.";
                }

                //Subida de la imagen 3
                if (FileUpload3.HasFile)
                {
                    subirImagen(product, FileUpload3);
                    lblSubida3.Text = "La imagen sa ha cargado con éxito.";
                }
            }
            //guardamos las excpeciones en la base de datos
            catch (Exception ex)
            {
             
            }

            //lblResult.Text = "El producto se ha guardado correctamente";
            //cmdSave.Enabled = false;

            //redirigimos al admin a la lista de productos para que compruebe el resultado
            Response.Redirect("AdminProductList");
        }

        /// <summary>
        ///Método que sube imágenes de productos al servidor y guarda su ruta en la base de datos
        /// </summary>
        public void subirImagen(Product product, FileUpload fileUpload)
        {
            //variables
            ApplicationDbContext context = new ApplicationDbContext();
            PictureManager pictureManager = new PictureManager(context);
            string fileName = "";
            Picture picture = null;

            //subimos la imagen al servidor
            fileName = Path.GetFileName(fileUpload.PostedFile.FileName);
            fileUpload.PostedFile.SaveAs(Server.MapPath("../../images/") + fileName);

            //creamos un objeto de la clase Imagen y lo añadimos a la base de datos
            picture = new Picture()
            {
                Route = fileName,
                ProductId = product.Id,
            };
            pictureManager.Add(picture);
            context.SaveChanges();
        }

    }
}
