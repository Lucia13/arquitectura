﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminCreateProduct.aspx.cs" Inherits="Muebles.Web.Admin.Products.AdminCreateProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Productos</h2>

    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

     <div class="form-horizontal">
        <h4 style="color: #591d1d">Crear un nuevo producto</h4>
        <hr />
         <asp:ValidationSummary  ID="ValidationSummary1" runat="server" CssClass="text-danger" />    
        
         <div class="form-group">
            <asp:Label ID="lblNombre" runat="server" Text="Nombre: "  CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtNombre" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNombre" ErrorMessage="El campo nombre es obligatorio."></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="lblDescripcion" runat="server" Text="Descripción:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtDescripcion" runat="server" Width="300px" CssClass="form-control" TabIndex="1" ></asp:TextBox>
                <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescripcion" ErrorMessage="El campo descripción es obligatorio."></asp:RequiredFieldValidator>
            </div>
        </div>    


         <div class="form-group">
            <asp:Label ID="lblPrecio" runat="server" Text="Precio:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtPrecio" runat="server" Width="100px" CssClass="form-control" TabIndex="2" ></asp:TextBox>
                <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPrecio" ErrorMessage="El campo precio es obligatorio." ></asp:RequiredFieldValidator>
                <asp:RangeValidator CssClass="text-danger" ID="RangeValidator2" runat="server" ErrorMessage="El precio no puede ser negativo."  MinimumValue="0" Type="Double" MaximumValue="30000" ControlToValidate="txtPrecio"></asp:RangeValidator>
            </div>
        </div>    

         <div class="form-group">
            <asp:Label ID="lblStock" runat="server" Text="Stock:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex; top: 0px; left: 0px;">
                <asp:TextBox ID="txtStock" runat="server" CssClass="form-control" TextMode="Number" Width="101px" TabIndex="3" ></asp:TextBox>
                <asp:RequiredFieldValidator CssClass="text-danger" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtStock" ErrorMessage="El campo stock es obligatorio." ></asp:RequiredFieldValidator>
                <asp:RangeValidator CssClass="text-danger" ID="RangeValidator1" runat="server" ErrorMessage="El stock no puede ser negativo."  MinimumValue="0" Type="Integer" MaximumValue="20000" ControlToValidate="txtStock"></asp:RangeValidator>
            </div>
        </div> 

         <div class="form-group">
               <div class="row">
                    <asp:Label ID="lblImagen" runat="server" Text="Imagen:" CssClass="col-md-2 control-label"></asp:Label>
                    <div class="col-md-10" style="display:inline-flex">
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="327px" TabIndex="4" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  CssClass="text-danger" runat="server" ErrorMessage="El campo imagen es obligatorio."  ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>
                        <asp:Label ID="lblSubida1" runat="server"></asp:Label>
                    </div>
               </div>

               <div class="row">
                   <div class="col-md-2"></div>
                     <div class="col-md-10" style="display:inline-flex">
                        <asp:FileUpload ID="FileUpload2" runat="server" Width="327px" TabIndex="5" />
                        <asp:Label ID="lblSubida2" runat="server"></asp:Label>
                     </div>
                       <br />

                 <div class="row" style="margin-top: 6px">
                </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-10" style="display:inline-flex">
                    <asp:FileUpload ID="FileUpload3" runat="server" Width="327px" TabIndex="6" />
                    <asp:Label ID="lblSubida3" runat="server"></asp:Label>
                    </div>
               </div>
        </div>  
        
          <div class="form-group">
            <asp:Label ID="lblCategoria" runat="server" Text="Categoría:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:DropDownList ID="ddlCategoria" runat="server"  CssClass="form-control" Width="327px" TabIndex="7" >       
                    </asp:DropDownList>  
            </div>
         </div>  

        <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <asp:Button ID="cmdSave" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" runat="server" Text="Guardar" OnClick="cmdSave_Click" CssClass="btn btn-default" TabIndex="8" />
            <asp:Label ID="lblResult" runat="server" ForeColor="#00CC00"></asp:Label>
        </div>
      </div>
    </div>

</asp:Content>

