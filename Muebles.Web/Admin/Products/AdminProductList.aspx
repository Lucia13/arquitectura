﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminProductList.aspx.cs" Inherits="Muebles.Web.Admin.Products.AdminProductList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <link href="<%: ResolveUrl("~/Content/DataTables/css/jquery.dataTables.css") %>" rel="stylesheet" />
    <script src="<%: ResolveUrl("~/Scripts/DataTables/jquery.dataTables.js") %>"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#MainContent_GridView1').DataTable(
                {
                    "oLanguage": { "sUrl": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json" },
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return "<a href='AdminEditProduct?productId=" + data + "'>" + data + "</a>";
                            },
                            "targets":0
                        },
                        { "title": "Nombre", "targets": 1 },
                        { "title": "Descripción", "targets": 2 },
                        { "title": "Precio", "targets": 3 },
                        { "title": "Stock", "targets": 4 },
                         { "title": "Categoría", "targets": 5 },
                    ]
                });

            $('#MainContent_GridView2').DataTable(
                {
                    "oLanguage": { "sUrl": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json" },
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return "<a href='AdminEditProduct?productId=" + data + "'>" + data + "</a>";
                            },
                            "targets": 0
                        },
                        { "title": "Nombre", "targets": 1 },
                        { "title": "Descripción", "targets": 2 },
                        { "title": "Precio", "targets": 3 },
                        { "title": "Stock", "targets": 4 },
                         { "title": "Categoría", "targets": 5 },
                    ]
                });
        });
    </script>

    <div style="margin-top:10px;margin-bottom:10px">
        <asp:Button CssClass="btn btn-default" ID="cmdNew" runat="server" Text="Nuevo producto" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" OnClick="cmdNew_Click"/> 
    </div>

    <h3 style="color: #591d1d">Productos disponibles</h3>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" >
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            <asp:BoundField DataField="Stock" HeaderText="Stock" SortExpression="Stock" />
            <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
        </Columns>
    </asp:GridView>
     
     <br />
     <h3 style="color: #591d1d">Productos agotados</h3>

    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource2" >
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            <asp:BoundField DataField="Stock" HeaderText="Stock" SortExpression="Stock" />
            <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
        </Columns>
</asp:GridView>
     
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetDataAvailable" TypeName="Muebles.Web.Admin.Products.AdminProductList"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetDataNotAvailable" TypeName="Muebles.Web.Admin.Products.AdminProductList"></asp:ObjectDataSource>
</asp:Content>

