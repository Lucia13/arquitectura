﻿using Muebles.Application;
using Muebles.CORE;
using Muebles.DAL;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace Muebles.Web.Admin.Products
{

    /// <summary>
    /// Clase de modificación y eliminación de productos por el administrador
    /// </summary>
    public partial class AdminEditProduct : System.Web.UI.Page
    {
        //variables estáticas
        ApplicationDbContext context = null;
        ProductManager productManager = null;
        Product product = null;

        /// <summary>
        ///Método de carga de la página
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            productManager = new ProductManager(context);

            int id;
            string productId = Request.QueryString["productId"];
            bool parse = int.TryParse(productId, out id);


            if (parse)
            {
                //buscamos el producto por id
                product = productManager.GetById(id);
            }

            if (!parse || product == null)
            {
                //Response.Redirect("../../Error");
            }
            if (!Page.IsPostBack)
            {
                
                //llenamos la caja desplegable de categorías
                ddlCategoria.DataSource = null;
                Dictionary<string, string> valuesEnum = new Dictionary<string, string>();
                var values = Enum.GetValues(typeof(Category));
                foreach (var value in values)
                {
                    valuesEnum.Add(((int)value).ToString(), value.ToString());
                }
                ddlCategoria.DataTextField = "Value";
                ddlCategoria.DataValueField = "Key";
                ddlCategoria.DataSource = valuesEnum;
                ddlCategoria.DataBind();

                //Mostramos los datos originales del producto
                if (product != null)
                {
                    txtNombre.Text = product.Name;
                    txtDescripcion.Text = product.Description;
                    txtPrecio.Text = product.Price.ToString();
                    txtStock.Text = product.Stock.ToString();
                    ddlCategoria.Text = product.Category.ToString();
                }
            }
        }

        /// <summary>
        ///Método que guarda cambios en el producto en la base de datos
        /// </summary>
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try { 
                product.Category = (Category)Enum.Parse(typeof(Category), ddlCategoria.SelectedValue);
                product.Name = txtNombre.Text;
                product.Description = txtDescripcion.Text;
                product.Price = float.Parse(txtPrecio.Text);
                product.Stock = int.Parse(txtStock.Text);

                context.SaveChanges();
            }
            //guardamos las excepciones en la base de datos
            catch (Exception ex)
            {
              
            }

            //lblResult.Text = "El producto se ha modificado con éxito.";

            //redirigimos al listado de productos dónde se puede ver el resultado
            Response.Redirect("AdminProductList");

        }

        /// <summary>
        ///Método que borra el producto de la base de datos
        /// </summary>
        protected void cmdDelete_Click(object sender, EventArgs e)
        {

            int id;
            string productId = Request.QueryString["productId"];
            bool parse = int.TryParse(productId, out id);
            if (parse)
            {
                productManager.RemoveById(id);
                context.SaveChanges();

                //lblResult.Text = "El producto se ha borrado con éxito.";

                //redirigimos al listado de productos dónde se puede ver el resultado
                Response.Redirect("AdminProductList");
            }
        }

    }
}
