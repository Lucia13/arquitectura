﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Muebles.DAL;
using Muebles.CORE;
using Muebles.Web.Models;
using Muebles.Application;

namespace Muebles.Web.Admin.Orders
{

    /// <summary>
    /// Clase de modificación de estado de pedido por el administrador
    /// </summary>
    public partial class AdminEditOrder : System.Web.UI.Page
    {

        //variables estáticas
        ApplicationDbContext context = null;
        OrderManager orderManager = null;
        Order order = null;


        /// <summary>
        /// Método de carga de la página
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {

            //incicialización de las variables
            context = new ApplicationDbContext();
            orderManager = new OrderManager(context);


            //llenamos el dropbobox de estados del pedido
            if (!Page.IsPostBack)
            {
                ddlEstado.DataSource = null;
                Dictionary<string, string> valuesEnum = new Dictionary<string, string>();
                var values = Enum.GetValues(typeof(Status));
                foreach (var value in values)
                {
                    valuesEnum.Add(((int)value).ToString(), value.ToString());
                }
                ddlEstado.DataTextField = "Value";
                ddlEstado.DataValueField = "Key";
                ddlEstado.DataSource = valuesEnum;
                ddlEstado.DataBind();
            }


            //buscamos el pedido por su id
            int id;
            string orderId = Request.QueryString["orderId"];
            bool parse = int.TryParse(orderId, out id);

            if (parse)
            {
                order = orderManager.GetById(id);
            }

            if (!Page.IsPostBack)
            {
                //Mostramos el estado actual del pedido
                if (order != null)
                {
                    ddlEstado.SelectedValue = order.Status.ToString();
                }
            }
        }

       
        /// <summary>
        /// //Método que guarda el cambio de estado del pedido
        /// </summary>
        protected void cmdSave_Click(object sender, EventArgs e)
        {

            order.Status = (Status)Enum.Parse(typeof(Status), ddlEstado.SelectedValue);

            //si se cancela el pedido, devolvemos los productos al almacén - el stock aumenta
            if (order.Status == Status.Cancelado)
            {
                ProductManager productManager = new ProductManager(context);
                OrderProductManager opManager = new OrderProductManager(context);
                List<OrderProduct> opLista = opManager.GetOPByOrderId(order.Id).ToList();

                foreach (OrderProduct op in opLista)
                {
                    Product producto = productManager.GetById(op.ProductId);
                    producto.Stock += op.Number;
                    context.SaveChanges();
                }
            }
            context.SaveChanges();

            //lblResult.Text = "El pedido se ha modificado con éxito.";
            Response.Redirect("AdminOrderList");
        }


    }
}

   

