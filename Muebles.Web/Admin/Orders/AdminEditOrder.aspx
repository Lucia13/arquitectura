﻿<%@ Page Title="AdminEditarUsuario" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminEditOrder.aspx.cs" Inherits="Muebles.Web.Admin.Orders.AdminEditOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="<%: ResolveUrl("~/Content/DataTables/css/jquery.dataTables.css") %>" rel="stylesheet" />
    <script src="<%: ResolveUrl("~/Scripts/DataTables/jquery.dataTables.js") %>"></script>

      <br /><br />

            <div class="form-group">
                <asp:Label ID="lblEstado" runat="server" AssociatedControlID="ddlEstado" CssClass="col-md-2 control-label">Estado</asp:Label>
                <div class="col-md-10" style="display:inline-flex">
                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control" TabIndex="12" Width="192px">
                    </asp:DropDownList>
                </div>
            </div>
      
    <br /><br />

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="cmdSave" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" runat="server" Text="Modificar" OnClick="cmdSave_Click" CssClass="btn btn-default"  />
                <asp:Label ID="lblResult" runat="server" ForeColor="#33CC33"></asp:Label>
            </div>
        </div>

    <script>
        $(document).ready(function(){
            $("#ddlEstado").focus(function(){
                $("#cmdSave").attr("CssClass", "btn btn-success");
            });
        });
    </script>

</asp:Content>
