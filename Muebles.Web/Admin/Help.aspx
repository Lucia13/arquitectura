﻿<%@ Page Title="Ayuda Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="Muebles.Web.Admin.Help" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
  <div class="container">
      <div class="page-header"><h1 style="color: #591d1d">Ayuda</h1></div>

        <div class="row">
            <div class="col-md-6">
            <h2 style="color: #591d1d">Productos</h2>

            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal1">Añadir un producto</button>

            <!-- Modal -->
            <div id="myModal1" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
       
                  </div>
                  <div class="modal-body">
                    <p>1. En el menú superior haga clic sobre Productos -&gt; Nuevo producto. (o Productos-&gt; Listado de productos-&gt; botón Nueva producto)</p>
                    <p>2. Rellene el nombre, la descripción, el precio y el stock del producto. Seleccione la ubicación de entre 1 y 3 imágenes del producto.<br />
                    En el&nbsp;desplegable eliga la categoría de producto.<br /></p>
                    <p>3. Pulse sobre Guardar.</p>
        
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div>
              </div>
            </div>
            <div>
            </div>


        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal2">Mostrar todos los productos</button>

        <!-- Modal -->
        <div id="myModal2" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
       
              </div>
              <div class="modal-body">
                <p> 1. En el menú superior haga clic sobre Productos -&gt; Listado de productos.<br />
                2. En la primera tabla puede ver los Productos son stock y en la segunda los sin stock.<br />
                3. Para buscar un producto inserte la palabra buscada en el cambo Buscar.<br />
                4. Para ordenar los productos haga clic en el campo de la cabecera de la tabla por el que desea ordenar los productos.<br /></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <div>
        </div>

        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal3">Modificar un producto</button>

        <!-- Modal -->
        <div id="myModal3" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
       
              </div>
              <div class="modal-body">
                <p>1. En el menú superior haga clic en Productos -&gt; Listado de productos. <br />
                2. Haga clic sobre el id del producto que desea modificar.<br />
                3. Modifique el nombre, la descripción, el precio o el stock del producto<br />
                4. Pulse sobre Guardar.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <div>
        </div>
         <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal4">Eliminar un producto</button>

        <!-- Modal -->
        <div id="myModal4" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
       
              </div>
              <div class="modal-body">
                <p> 1. En el menú superior haga clic en Productos -&gt; Listado de productos <br />
                2. Haga clic sobre el id del producto que desea eliminar.<br />
                3. Pulse sobre Eliminar.
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
      </div>
  </div>

  <div class="col-md-6">
      <h2 style="color: #591d1d">Pedidos</h2>
        
         <!-- Trigger the modal with a button -->
     <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal5">Mostrar todos los pedidos</button>

        <!-- Modal -->
        <div id="myModal5" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
       
              </div>
              <div class="modal-body">
                <p>  1. En el menú superior haga clic en Pedidos.<br />
                Para buscar un pedido inserte la palabra buscada en el cambo Buscar.<br />
                Para ordenar los pedidos haga clic en el campo de la cabecera de la tabla por el que desea ordenar los pedidos
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
         <div>
        </div>

                 <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal6">Mostrar los productos de un pedido</button>

        <!-- Modal -->
        <div id="myModal6" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
       
              </div>
              <div class="modal-body">
                <p> 1. En el menú superior haga clic en Pedido. <br />
                2. Haga clic sobre el id del pedido cuyos productos desea ver.
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <div>
        </div>

                 <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal7">Modificar el estado de un pedido</button>

        <!-- Modal -->
        <div id="myModal7" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
       
              </div>
              <div class="modal-body">
                <p>  1. En el menú superior haga clic en Pedido. <br />
                2. Haga clic sobre el id del pedido que desea modificar.<br />
                3. Modifique el estado del pedido el la caja despegable Estado. <br />
                4. Pulse sobre Guardar.
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <div>
        </div>

                 <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal8">Eliminar un pedido</button>

        <!-- Modal -->
        <div id="myModal8" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
       
              </div>
              <div class="modal-body">
                <p>1. En el menú superior haga clic en Pedido. <br />
                2. Haga clic sobre el id del pedido que desea modificar.<br />
                3. Seleccione en la caja despegable Estado la opción Cancelado. <br />
                4. Pulse sobre Guardar.
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
     </div>
  </div>
</div>
        </asp:Content>

