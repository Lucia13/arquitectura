﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.CompensatingResourceManager;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Muebles.Web.User
{

    /// <summary>
    /// Clase que permite al usuario pagar intorduciendo los datos de la tarjeta de crédito
    /// </summary>
    public partial class Payment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Address");
        }

        /// <summary>
        /// Método que comprueba la validez de los datos de la tarjeta de crédito
        /// </summary>
        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            //si son correctos
            if ((Nombre.Text.Equals("Lucia Daubnerova") && Numero.Text.Equals("0000 0000 0000 0000") && CCV.Text.Equals("000") && Caducidad1.Text.Equals("03/23")) ||
                (Nombre.Text.Equals("Alberto Molinero Fernandez") && Numero.Text.Equals("3333 3333 3333 3333") && CCV.Text.Equals("333") && Caducidad1.Text.Equals("03/24")))
            {
                Response.Redirect("Resume");
            }
            //si son incorrectos
            else
            {
                PlaceHolder1.Visible = true;

            }

        }
    }
}