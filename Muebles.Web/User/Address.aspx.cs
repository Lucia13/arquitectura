﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Muebles.Application;
using Muebles.CORE;
using Muebles.DAL;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;


namespace Muebles.Web.User
{

    /// <summary>
    /// Clase que permite al usuario modificar la dirección de entrega antes de pagar
    /// </summary>
    public partial class Address : System.Web.UI.Page
    {

        ApplicationUser user = null;

        //Método de carga de la página
        protected void Page_Load(object sender, EventArgs e)
        {

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();


            //buscamos al usuario por id
            string userId = User.Identity.GetUserId();
            user = manager.FindById(userId);

            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
            AddressManager addressMananager = new AddressManager(context);
            CORE.Address address = addressMananager.GetByUser(userId); //dirección del usuario

            if (!Page.IsPostBack)
            {
                //Mostramos los datos originales del cliente
                Nombre.Text = user.Name;
                Apellidos.Text = user.Surnames;
                Calle.Text = address.Street;
                Provincia.Text = address.Province;
                CP.Text = address.CP;
                Ciudad.Text = address.City;
                Email.Text = user.Email;
                Telefono.Text = user.PhoneNumber;

                //Llenamos la caja desplegable de países
                ddlPais.DataSource = null;
                Dictionary<string, string> valuesEnum = new Dictionary<string, string>();
                var values = Enum.GetValues(typeof(Country));
                foreach (var value in values)
                {
                    valuesEnum.Add(((int)value).ToString(), value.ToString());
                }
                ddlPais.DataTextField = "Value";
                ddlPais.DataValueField = "Key";
                ddlPais.DataSource = valuesEnum;
                ddlPais.DataBind();

                ddlPais.SelectedValue = address.Country.ToString();
            }
        }


        /// <summary>
        /// Método que guarda cambios en los datos del usuario
        /// </summary>
        protected void Confirmar_Click(object sender, EventArgs e)
        {


            DAL.ApplicationDbContext context = new DAL.ApplicationDbContext();
      

            try
            {
                user.UserName = Email.Text;
                user.Email = Email.Text;
                user.PhoneNumber = Telefono.Text;
                user.Name = Nombre.Text;
                user.Surnames = Apellidos.Text;
              

                context.SaveChanges();
            }

            //guardamos excepciones en la base de datos
            catch (Exception ex)
            {
                
            }

            Response.Redirect("Payment");

        }


        /// <summary>
        /// Método que redirige a la página anterior
        /// </summary>
        protected void Volver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Cart");
        }
    }
}
