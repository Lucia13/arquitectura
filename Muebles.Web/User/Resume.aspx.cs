﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Muebles.Application;
using Muebles.CORE;
using Muebles.DAL;
using Muebles.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.UI.WebControls;

namespace Muebles.Web.User
{

    /// <summary>
    /// Clase que muestra el resúmen del pedido y completa el pedido enviando una copia al cliente y otra al vendedor
    /// </summary>
    public partial class Resume : System.Web.UI.Page
    {
        //Método de carga de la página
        protected void Page_Load(object sender, EventArgs e)
        {

            //establecemos cabeceras de las tablas
            GridView1.UseAccessibleHeader = true;
            if (GridView1.HeaderRow != null)
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;

            //contexto
            ApplicationDbContext context = new ApplicationDbContext();

            //managers
            OrderManager orderManager = new OrderManager(context);
            OrderProductManager opManager = new OrderProductManager(context);
            ProductManager productManager = new ProductManager(context);

            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            //listas
            List<OrderProduct> listaOP = new List<OrderProduct>();
            List<CartList> lista = new List<CartList>();
            float total = 0.0f;

            string articulo = ""; //string de los productos del pedido

            try
            {
                //buscamos el pedido
                Order pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId());
 
                if (pedido != null)
                {

                    //cambiamos el estado del pedido a pagado
                    pedido.Status = Status.Pagado;

                    Product producto = new Product();

                    //obtenemos los productos del pedido y creamos un string con sus datos que enviaremos por correo
                    listaOP = opManager.GetOPByOrderId(pedido.Id).ToList();
                    foreach (OrderProduct op in listaOP)
                    {
                        producto = productManager.GetById(op.ProductId);
                        articulo += producto.Name + " x " + op.Number + " " + (producto.Price * op.Number) + "€ <br />";
                        total += (producto.Price * op.Number);
                        //bajamos el stock de los productos comprados
                        producto.Stock-=op.Number; 
                        
                    }
                    pedido.Total = total;
                    context.SaveChanges();
                    lblTotal.Text = total.ToString();
                }

                

                //Enviamos una copia del pedido por correo al cliente y al vendedor

                System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

                //Direcciones de correo electrónico a las que queremos enviar el mensaje
                mmsg.To.Add(new MailAddress("luciadaubnerova13@gmail.com")); //vendedor ficticio

                ApplicationUser usuario = userManager.Users.Where(u => u.Id == pedido.User_Id).Single();
                mmsg.To.Add(new MailAddress(usuario.Email)); //cliente

                //Correo electronico desde la que enviamos el mensaje
                mmsg.From = new System.Net.Mail.MailAddress("luciadaubnerova13@gmail.com");
                //Asunto
                mmsg.Subject = "MiCasa - Copia del pedido";
                mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

                //Cuerpo del Mensaje - copia del pedido
                mmsg.Body = "x";
                mmsg.BodyEncoding = System.Text.Encoding.UTF8;
                mmsg.IsBodyHtml = true; //Queremos que se envíe como HTML


                /*-------------------------CLIENTE DE CORREO----------------------*/

                //Creamos un objeto de cliente de correo
                System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

                //Hay que crear las credenciales del correo emisor
                cliente.UseDefaultCredentials = false;
                cliente.Credentials = new System.Net.NetworkCredential("luciadaubnerova13@gmail.com", "paulsa14");

                //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail
                cliente.Port = 587;
                cliente.EnableSsl = true;

                //Para Gmail
                cliente.Host = "smtp.gmail.com";


                /*-------------------------ENVIO DE CORREO----------------------*/
                //Enviamos el mensaje      
                cliente.Send(mmsg);
                mmsg.Dispose();
            }
            //guardamos las excepciones en la base de datos
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Metodo que retorna los datos del pedido para la datatable
        /// </summary>
        /// <returns>Lista de los productos del pedido </returns>
        public List<CartList> GetData()
        {

            //variables
            float total = 0.0f;
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager orderManager = new OrderManager(context);
            OrderProductManager opManager = new OrderProductManager(context);
            ProductManager productManager = new ProductManager(context);
            List<OrderProduct> listaOP = new List<OrderProduct>();
            List<CartList> lista = new List<CartList>();

            Order pedido = new Order();
            try
            {
                //buscamos el pedido
                pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId());

                //creamos una lista con los datos de los productos pedidos
                if (pedido != null)
                {
                    Product producto = new Product();
                    listaOP = opManager.GetOPByOrderId(pedido.Id).ToList();
                    foreach (OrderProduct op in listaOP)
                    {
                        CartList cesta = new CartList();
                        producto = productManager.GetById(op.ProductId);
                        cesta.Name = producto.Name;
                        cesta.Number = op.Number;
                        cesta.Price = producto.Price * cesta.Number;
                        total += cesta.Price;
                        lista.Add(cesta);
                    }
                }
            }
            //guardamos las excepciones en la base de datos
            catch (Exception ex)
            {
               
            }

            return lista;
        }
    
    }
}