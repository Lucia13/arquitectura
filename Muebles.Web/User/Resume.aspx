﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Resume.aspx.cs" Inherits="Muebles.Web.User.Resume" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="<%: ResolveUrl("~/Content/DataTables/css/jquery.dataTables.css") %>" rel="stylesheet" />
    <script src="<%: ResolveUrl("~/Scripts/DataTables/jquery.dataTables.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#MainContent_GridView1').DataTable(
                {
                    "oLanguage": { "sUrl": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json" },
                    "columnDefs": [
                        
                        { "title": "Producto", "targets": 0 },
                        { "title": "Cantidad", "targets": 1 },
                        { "title": "Precio", "targets": 2 },
                    ]
                });
           
        });
    </script>

    <div class="container">
        <br />

        <div class="alert alert-success fade in alert-dismissible text-center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>¡Felicidades!</strong> La compra se ha realizado correctamente.
        </div>

        <div class="page-header"><h2 style="color: #591d1d">Resumen de su pedido</h2></div>

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" >
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Number" HeaderText="Number" SortExpression="Number" />
                <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            </Columns>
        </asp:GridView>

         <asp:Label ID="lblTotal0" runat="server" Text="Total: "></asp:Label>
         <span class="badge"><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></span>
         <asp:Label ID="Label1" runat="server" Text=" €"></asp:Label>

          <ul class="pager">
          <li><a href="../Default" data-toggle="tooltip" title="Se abre la página Inicio" style="border: thin solid #591d1d; color: #591d1d; background-color: #E6BCCF; font-weight: bold;"><span class="glyphicons glyphicons-home"></span>Página principal</a></li>
          </ul>
    
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="Muebles.Web.User.Resume"></asp:ObjectDataSource>
    </div>

    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip(); 
        });
    </script>

</asp:Content>

