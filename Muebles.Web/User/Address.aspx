﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Address.aspx.cs" Inherits="Muebles.Web.User.Address" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="container">
    <div class="page-header"><h4 style="color: #591d1d">Compruebe y confirme los datos de envío y contacto, por favor</h4></div>
       <div class="form-horizontal">
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
       

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Nombre" CssClass="col-md-2 control-label">Nombre</asp:Label>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Nombre"
                    CssClass="text-danger" ErrorMessage="El campo nombre es obligatorio." />
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Nombre" CssClass="form-control"  />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Apellidos" CssClass="col-md-2 control-label">Apellidos</asp:Label>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Apellidos"
                    CssClass="text-danger" ErrorMessage="El campo apellidos es obligatorio." />
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Apellidos" CssClass="form-control"  />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Calle" CssClass="col-md-2 control-label">Calle</asp:Label>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Calle"
                    CssClass="text-danger" ErrorMessage="El campo calle es obligatorio." />
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Calle" CssClass="form-control" />
            </div>
        </div>
        
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Ciudad" CssClass="col-md-2 control-label">Ciudad</asp:Label>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Ciudad"
                    CssClass="text-danger" ErrorMessage="El campo ciudad es obligatorio." />
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Ciudad" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Provincia" CssClass="col-md-2 control-label">Provincia</asp:Label>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Provincia"
                    CssClass="text-danger" ErrorMessage="El campo provincia es obligatorio." />
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Provincia" CssClass="form-control"  />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="CP" CssClass="col-md-2 control-label">Código postal</asp:Label>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="CP"
                    CssClass="text-danger" ErrorMessage="El campo código postal es obligatorio." />
          <div class="col-md-10">
                <asp:TextBox runat="server" ID="CP" CssClass="form-control"  />
          </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ddlPais" CssClass="col-md-2 control-label">País</asp:Label>
            <div class="col-md-10">
               <asp:DropDownList ID="ddlPais" runat="server"  CssClass="form-control" Width="180px">       
                </asp:DropDownList>
            </div>
        </div>

         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="El campo email es obligatorio." />
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
            </div>
        </div>

          <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Telefono" CssClass="col-md-2 control-label">Teléfono</asp:Label>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Telefono"
                    CssClass="text-danger" ErrorMessage="El campo teléfono es obligatorio." />
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Telefono" CssClass="form-control" />
            </div>
        </div>
    </div>
  </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="Volver_Click" BackColor="#d1cdcc" BorderColor="#591d1d" ForeColor="#591d1d" Text="Volver" CssClass="btn btn-default" /><span>&nbsp;&nbsp;</span>
                <asp:Button runat="server" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" OnClick="Confirmar_Click" Text="Confirmar" CssClass="btn btn-default" />
            </div>
        </div>

</asp:Content>

