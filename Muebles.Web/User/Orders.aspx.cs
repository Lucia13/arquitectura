﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Muebles.DAL;
using Muebles.Application;
using Muebles.Web.Models;
using Muebles.CORE;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;


namespace Muebles.Web.User
{

    /// <summary>
    /// Clase para listar todos los pedidos del usuario
    /// </summary>
    public partial class Orders : System.Web.UI.Page
    {
        /// <summary>
        /// Método de carga de la página
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {

            //establecemos la cabecera de la tabla
            GridView1.UseAccessibleHeader = true;
            if (GridView1.HeaderRow != null)
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        /// <summary>
        /// Método que llena la tabla de pedidos
        /// </summary>
        /// <returns>lista de pedidos</returns>
        public List<OrderList> GetData()
        {
            //managers
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager orderManager = new OrderManager(context);
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            List<Order> lista = orderManager.GetAllByUserId(User.Identity.GetUserId()).ToList();

            if (lista.Count != 0)
            {

                //obtenemos todos los pedidos y seleccionamos los datos que queremos mostrar en la datatable
                var pedidos = orderManager.GetAll().ToList().Select(i => new OrderList()
                {
                    Id = i.Id, //id del pedido
                    CreatedDate = i.CreatedDate, //fecha de creación
                    Status = i.Status,
                    Total = i.Total //total a pagar
                });

                return pedidos.ToList();
            }
            else
            {
                return null;
            }
        }

    }
}