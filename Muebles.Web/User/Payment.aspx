﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="Muebles.Web.User.Payment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="page-header"><h2 style="color: #591d1d">Pago</h2></div>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="container">

       <div class="well">Introduzca los datos de su tarjeta de crédito</div>

       <asp:PlaceHolder ID="PlaceHolder1" Visible="false" runat="server">
            <div  id="alert" class="alert alert-danger fade in alert-dismissible text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                 <strong>¡Lo sentimos!</strong> Los datos de pago son incorrectos.
            </div>
       </asp:PlaceHolder>

        <div class="form-horizontal">

            <hr />
            <asp:ValidationSummary runat="server" CssClass="text-danger" />
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Nombre" CssClass="col-md-2 control-label">Nombre del titular</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Nombre" CssClass="form-control" Width="230px" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Nombre"
                        CssClass="text-danger" ErrorMessage="El campo nombre del titular es obligatorio." />
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Numero" CssClass="col-md-2 control-label">Número de la tarjeta</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Numero" CssClass="form-control" Width="235px" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Numero"
                        CssClass="text-danger" ErrorMessage="El campo número de la tarjeta es obligatorio." />
                </div>
            </div>

               <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="CCV" CssClass="col-md-2 control-label">CCV</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="CCV" CssClass="form-control" Width="85px" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CCV"
                        CssClass="text-danger" ErrorMessage="El campo CCV  es obligatorio." />
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Caducidad1" CssClass="col-md-2 control-label">Caducidad</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Caducidad1" Text="/" CssClass="form-control" Width="85px" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Caducidad1"
                    CssClass="text-danger" ErrorMessage="El campo caducidad es obligatorio." />
            
                </div>
            </div>

             <span class="label-danger"><asp:Label runat="server" ID="lblResult"  CssClass="col-md-2 control-label"></asp:Label></span>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                     <asp:Button runat="server" ID="btnVolver" CssClass=" btn btn-default btn-md" BorderColor="#591D1D" Font-Bold="True" BackColor="#d1cdcc" ForeColor="#591d1d" OnClick="btnVolver_Click" Text="Volver" /><span>&nbsp; </span>
                    <asp:Button runat="server"  text="Confirmar" CssClass=" confirmar btn btn-default btn-md" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" OnClick="btnConfirmar_Click" /><span class="glyphicon glyphicons-circle-arrow-right"></span>   
                </div>
            </div>
          </div>
   </div>

</asp:Content>


