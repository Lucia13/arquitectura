﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="Muebles.Web.User.Orders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <link href="<%: ResolveUrl("~/Content/DataTables/css/jquery.dataTables.css") %>" rel="stylesheet" />
    <script src="<%: ResolveUrl("~/Scripts/DataTables/jquery.dataTables.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#MainContent_GridView1').DataTable(
                {
                    "oLanguage": { "sUrl": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json" },
                    "columnDefs": [
                       
                        { "title": "Id", "targets": 0 },
                        { "title": "Fecha", "targets": 1 },
                        { "title": "Estado", "targets": 2 },
                        { "title": "Total €", "targets": 3 },
                    ]
                });
        });
    </script>

    <div style="margin-top:10px;margin-bottom:10px">
        <div class="page-header"><h1 style="color: #591d1d">Mis pedidos</h1></div>
    </div>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
            <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" />
        </Columns>
   </asp:GridView>

<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="Muebles.Web.User.Orders"></asp:ObjectDataSource>


</asp:Content>
