﻿using Microsoft.AspNet.Identity;
using Muebles.Application;
using Muebles.CORE;
using Muebles.DAL;
using Muebles.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Muebles.Web.User
{
    /// <summary>
    /// Clase que muestra los productos pedidos por el usuario
    /// </summary>
    public partial class Cart : System.Web.UI.Page
    {
        //Método de carga de la página
        protected void Page_Load(object sender, EventArgs e)
        {

  

            //declaración e inicialización de variables
            float total = (float)0.0;
            int cont = 0;
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager orderManager = new OrderManager(context);
            OrderProductManager opManager = new OrderProductManager(context);
            ProductManager productManager = new ProductManager(context);
            List<OrderProduct> listaOP = new List<OrderProduct>();
            Order pedido = new Order();

            //buscamos el pedido del usuario
            pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId());

            if (pedido != null)
            {
                Product producto = new Product();

                //buscamos los productos del pedido
                listaOP = opManager.GetOPByOrderId(pedido.Id).ToList();

                //para cada producto creamos una fila
                foreach (OrderProduct op in listaOP)
                {
                    TableRow fila = new TableRow();

                    //celda nombre
                    TableCell item = new TableCell();
                    producto = productManager.GetById(op.ProductId);
                    item.Text = producto.Name;

                    // celda cantidad
                    TableCell item2 = new TableCell();
                    item2.Text = op.Number.ToString();

                    // celda precio
                    TableCell item3 = new TableCell();
                    item3.Text = (producto.Price * op.Number).ToString();

                    // celda botón Eliminar
                    TableCell item4 = new TableCell();
                    Button btnEliminar = new Button();
                    btnEliminar.ID = "btnAnadir" + cont;
                    btnEliminar.CssClass = "btn btn-danger btn-md";
                    btnEliminar.Click += new EventHandler((sender1, e1) => btnEliminar_Click(sender1, e1, op));
                    btnEliminar.Text = "Borrar";


                    item4.Controls.Add(btnEliminar);

                    //añadimos las celdas en las filas
                    fila.Controls.Add(item);
                    fila.Controls.Add(item2);
                    fila.Controls.Add(item3);
                    fila.Controls.Add(item4);
                    Table1.Controls.Add(fila);

                    cont++;
                    //total a pagar
                    total += float.Parse(item3.Text);
                }

                lblTotal.Text = total.ToString();
            }
         }

        /// <summary>
        /// Método que elimina producto del carrito
        /// </summary>
        protected void btnEliminar_Click(Object sender1, EventArgs e1, OrderProduct op){

            ApplicationDbContext context = new ApplicationDbContext();
            OrderProductManager opManager = new OrderProductManager(context);

            try { 
            //eliminación
            opManager.RemoveById(op.Id);
            context.SaveChanges();
            }

            //guardamos excepciones a la base de datos
            catch (Exception ex)
            {
              
            }
            lblResult.Text = "El producto se ha eliminado con éxito.";
            Response.Redirect("Cart");
        }

        /// <summary>
        /// Metodo que retorna los productos del pedido
        /// </summary>
        /// <returns>Lista de productos del pedido </returns>
        public List<CartList> GetData()
        {
            float total = (float)0.0;

            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager orderManager = new OrderManager(context);
            OrderProductManager opManager = new OrderProductManager(context);
            ProductManager productManager = new ProductManager(context);

            List<CartList> lista = new List<CartList>();
            List<OrderProduct> listaOP = new List<OrderProduct>();
            Order pedido = new Order();

            //obetenemos el ultimo pedido del usuario
            pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId());

            if (pedido != null)
            {
                Product producto = new Product();
                //obtenemos los productos del pedido
                listaOP = opManager.GetOPByOrderId(pedido.Id).ToList();

                //llenamos la lista de los datos de los productos pedidos
                foreach (OrderProduct op in listaOP)
                {
                    CartList cesta = new CartList();
                    producto = productManager.GetById(op.ProductId);
                    cesta.Name = producto.Name;
                    cesta.Number = op.Number;
                    cesta.Price = producto.Price * cesta.Number;
                    total += cesta.Price;
                    lista.Add(cesta);
                }

                //asignamos suma total a pagar al pedido
                pedido.Total = total;
                context.SaveChanges();
            }
            return lista;

        }

        //Al hacer click sobre Añadir productos volvemos al catálogo de productos
        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("../ProductList");
        }


        //Al hacer click sobre Pagar se abre nuevo formulario
        protected void btnPagar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Address");
        }



    }
}