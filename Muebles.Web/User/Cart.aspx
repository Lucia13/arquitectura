﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="Muebles.Web.User.Cart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<link href="<%: ResolveUrl("~/Content/DataTables/css/jquery.dataTables.css") %>" rel="stylesheet" />
<script src="<%: ResolveUrl("~/Scripts/DataTables/jquery.dataTables.js") %>"></script>
   
   <h3 style="color: #591d1d">Carrito de la compra</h3>

  <div class="table-responsive">
    <asp:Table ID="Table1" 
            CssClass="table table-striped"
            Width="100%"
            runat="server"  
            BorderWidth="2"
            CellPadding="5"
            CellSpacing="5">
            <asp:TableHeaderRow 
                runat="server" 
                Font-Bold="true"
                >
                <asp:TableHeaderCell>Nombre</asp:TableHeaderCell>
                <asp:TableHeaderCell>Cantidad</asp:TableHeaderCell>
                <asp:TableHeaderCell>Precio</asp:TableHeaderCell>
                 <asp:TableHeaderCell>Eliminar</asp:TableHeaderCell>
            </asp:TableHeaderRow>
     </asp:Table>
  </div>
 
   <asp:Label ID="Label2" runat="server" Text="Total: "></asp:Label>
   <span class="badge"><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></span>
   <asp:Label ID="Label1" runat="server" Text=" €"></asp:Label>
   <br />

   <asp:Label ID="lblResult" runat="server" ForeColor="#009933"></asp:Label>
   
    <div style="margin-top:10px;margin-bottom:10px">
          <asp:Button CssClass="btn btn-default"  BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" ID="cmdNew" runat="server" Text="Añadir productos" OnClick="btnNuevo_Click" Width="166px"/>   
          <asp:Button CssClass="btn btn-default" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" ID="btnPagar" runat="server" Text="Pagar" OnClick="btnPagar_Click" Width="204px"/>   
     </div>

    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="Muebles.Web.User.Cart"></asp:ObjectDataSource>

</asp:Content>
