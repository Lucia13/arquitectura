﻿<%@ Page Title="Ayuda" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="Muebles.Web.User.Help" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <div class="page-header"><h1 style="color: #591d1d">Ayuda</h1>
        </div>

        <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #E6BCCF"><h3 class="text-center" style="color: #591d1d">Comprar un producto</h3>
                </div>
                <div class="panel-body">
                    1. En el menú superior haga clic en Registro y rellene todos los datos.<br />
                    2. Pulse sobre Registrar.<br />
                    3. En el menú superior elija la categoría del producto que busca (Cocina, Salón o Dormitorio).<br />
                    4. Seleccione la cantidad del producto deseado y pulse sobre Añadir. Repita la acción para todos los productos que desee.<br />
                    5. Pulse sobre Comprar abajo.<br />
                    6. Revise los datos del pedido.
                    <br />
                    Si no quiere alguno de los productos pedidos, pulse sobre Eliminar al lado del producto.<br />
                    7. Pulse sobre continuar.<br />
                    8. Revise los datos de entrega y si es necesario, cámbielos.<br />
                    9. Pulse sobre Continuar.<br />
                    10. Introduzca los datos de su tarjeta de crédito y pulse sobre Pagar.<br />
                    11. Verá el resúmen del pedido y por correo electrónico recibirá una copia del pedido.<br />
                    12. Recibirá el artículo dentro de 7 días en la dirección indicada.
                </div>
        </div>
  </div>

</asp:Content>

