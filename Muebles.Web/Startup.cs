﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute("Muebles.WebConfig", typeof(Muebles.Web.Startup))]

namespace Muebles.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
