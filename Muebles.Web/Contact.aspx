﻿<%@ Page Title="Contacto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Muebles.Web.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   
  
    <div class="container">
         <div class="page-header"><h1 style="color: #591d1d"><%: Title %></h1></div> 
          <div class="row">
              <div class="col-md-4">
                    <address>
                        <b><u>Dirección</u></b><br />
                        <span class="glyphicon glyphicon-map-marker"></span>C. França 33, 1 2<br />
                        080 24<br />Barcelona<br />
                        <span class="glyphicon glyphicon-phone"></span>684316501<br />
                        <span class="glyphicon glyphicon-envelope"></span>luciadaubnerova13@gmail.com<br />
                    </address>
               </div>
              <div class="col-md-8">
                  <b><u>Abierto</u></b><br />
                  Lu 10:00-20:00<br />
                  Ma 10:00-20:00<br />
                  Mi 10:00-20:00<br />
                  Ju 10:00-20:00<br />
                  Vi 10:00-20:00<br />
            </div>
    </div>   
</div>
    <!-- Set height and width with CSS -->
<div id="googleMap" style="height: 550px;width: 800px; margin-top: 15px;"></div>




<!-- Add Google Maps -->
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
    var myCenter = new google.maps.LatLng(41.4158201, 2.1630082000000357); 41.4155148, 2.154015

    function initialize() {
        var mapProp = {
        center:myCenter,
        zoom:16,
        scrollwheel:false,
        draggable:false,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var marker = new google.maps.Marker({
        position:myCenter,
    });

    marker.setMap(map);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

 
</asp:Content>
