﻿using Muebles.Application;
using Muebles.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Muebles.CORE;
using System.Web.UI.HtmlControls;
using System.Drawing;

namespace Muebles.Web
{

    ///<summary>
    ///Clase que muestra los productos de la categoría cocina y permite añadirlos al carrito
    ///</summary>
    public partial class ProductList : System.Web.UI.Page
    {

        //Método que muestra los productos por pantalla
        protected void Page_Load(object sender, EventArgs e)
        {

            List<Product> list = new List<Product>();
            list = GetData(Request.QueryString["categoria"]);
            int cont = 0;

            if (list.Count != 0)
            {
                foreach (Product producto in list)
                {
               
                    //panel contenedor
                    Panel panel = new Panel();
                    panel.ID = "pnl" + cont;
                    panel.Width = 400;
                    panel.BorderWidth = 1;
                    panel.BorderColor = Color.Brown;
                    panel.BorderStyle = BorderStyle.Solid;

                    //nombre de producto
                    Label lblNombre = new Label();
                    lblNombre.ID = "lblNombre" + cont;
                    lblNombre.Font.Bold = true;
                    lblNombre.Font.Size = 20;
                    lblNombre.Font.Underline = true;
                    lblNombre.ForeColor = Color.Brown;
                    lblNombre.Text = "&nbsp;" + producto.Name;
                    panel.Controls.Add(lblNombre);

                    //espacio
                    HtmlGenericControl br = new HtmlGenericControl("br");
                    panel.Controls.Add(br);

                    //imágenes
                    List<Picture> listPictures = new List<Picture>();
                    listPictures = GetDataPictures(producto.Id);

                    foreach (Picture picture in listPictures)
                    {

                        System.Web.UI.WebControls.Image img = new System.Web.UI.WebControls.Image();
                        img.ID = "img" + cont;
                        img.ImageUrl = "images/" + picture.Route;
                        img.CssClass = "img-rounded img-responsive img-thumbnail";
                        img.Width = 300;
                        img.Height = 280;
                        panel.Controls.Add(img);
                        cont++;
                        //espacio
                        HtmlGenericControl brPic = new HtmlGenericControl("br");
                        panel.Controls.Add(brPic);
                    }

                    //descripción
                    Label lblDescripcion = new Label();
                    lblDescripcion.ID = "lblDescripcion" + cont;
                    lblDescripcion.CssClass = "control-label";
                    lblDescripcion.Text = "&nbsp;" + producto.Description;
                    panel.Controls.Add(lblDescripcion);

                    //espacio
                    HtmlGenericControl br2 = new HtmlGenericControl("br");
                    panel.Controls.Add(br2);

                    //precio
                    Label lblPrecio = new Label();
                    lblPrecio.ID = "lblPrecio" + cont;
                    lblPrecio.CssClass = "control-label";
                    lblPrecio.ForeColor = Color.Green;
                    lblPrecio.Font.Bold = true;
                    lblPrecio.Text = "&nbsp;" + producto.Price.ToString("####0.00") +" €";
                    panel.Controls.Add(lblPrecio);

                    //espacio
                    HtmlGenericControl br3 = new HtmlGenericControl("br");
                    panel.Controls.Add(br3);

                    //si el producto está agotado, informamos al usuario
                    if (producto.Stock == 0)
                    {
                        //label sin stock
                        Label lblStock = new Label();
                        lblStock.ID = "lblStock" + cont;
                        lblStock.CssClass = "text-danger";
                        lblStock.Text = "&nbsp;Sin stock";
                        panel.Controls.Add(lblStock);

                        //espacio
                        HtmlGenericControl br4 = new HtmlGenericControl("br");
                        panel.Controls.Add(br4);
                    }

                    //si no está agotado
                    else
                    {
                        //label cantidad
                        Label lblCantidad = new Label();
                        lblCantidad.ID = "lblCantidad" + cont;
                        lblCantidad.CssClass = "control-label";
                        lblCantidad.ForeColor = Color.Brown;
                        lblCantidad.Font.Bold = true;
                        lblCantidad.Text = "&nbsp;Cantidad";
                        panel.Controls.Add(lblCantidad);

                        //campo cantidad
                        TextBox txtCantidad = new TextBox();
                        txtCantidad.ID = "txtCantidad" + cont;
                        txtCantidad.CssClass = "form-control";
                        txtCantidad.Width = 50;
                        txtCantidad.Text = "1";
                        panel.Controls.Add(txtCantidad);

                        //espacio
                        HtmlGenericControl br5 = new HtmlGenericControl("br");
                        panel.Controls.Add(br5);

                        //si se trata de un usuario registrado, mostramos el botón Añadir al carrito
                        if (User.Identity.GetUserId() != null)
                        {
                            //botón Añadir
                            Button btnAnadir = new Button();
                            btnAnadir.ID = "btnAnadir" + cont;
                            btnAnadir.CssClass = "btn btn-success btn-md";
                            btnAnadir.Click += new EventHandler((sender1, e1) => btnAnadir_Click(sender1, e1, producto, int.Parse(txtCantidad.Text)));
                            btnAnadir.Text = "Añadir";
                            panel.Controls.Add(btnAnadir);

                            //espacio
                            HtmlGenericControl br6 = new HtmlGenericControl("br");
                            panel.Controls.Add(br6);
                        }
                    }

                    ph1.Controls.Add(panel);

                    HtmlGenericControl br7 = new HtmlGenericControl("br");
                    ph1.Controls.Add(br7);

                    cont++;
                }
            }
            else
            {
                lblVacio.Visible = true;
            }

            HtmlGenericControl brFin = new HtmlGenericControl("br");
            ph1.Controls.Add(brFin);
       

            //si se trata de un usuario registrado, mostramos el botón Comprar
            if (User.Identity.GetUserId() != null && list.Count!=0)
            {
                Button cmdSave = new Button();
                cmdSave.ID = "cmdSave" + cont;
                cmdSave.CssClass = "btn btn-success btn-lg";
                cmdSave.Click += new EventHandler((sender1, e1) => cmdSave_Click(sender1, e1));
                cmdSave.Text = "Comprar";
                ph1.Controls.Add(cmdSave);
            }
        }


        //Método que añade producto al carrito de la compra
        private void btnAnadir_Click(object sender, EventArgs e, Product producto, int cantidad)
        {
            //informamos al usuario de que el producto se ha añadido
            phAlert.Visible = true;

            //variables
            ApplicationDbContext context = new ApplicationDbContext();
            OrderManager orderManager = new OrderManager(context);
            OrderProductManager opManager = new OrderProductManager(context);

            try
            {
                //buscamos pedidos pendientes del usuario
                Order pedido = orderManager.GetByUserIdTodayPending(User.Identity.GetUserId());

                // si existe un pedido no pagado del mismo día
                if (pedido != null)
                {
                }
                //si no existe pedido no pagado del mismo día, creamos un pedido nuevo
                else
                {
                    pedido = new Order()
                    {
                        CreatedDate = DateTime.Now,
                        User_Id = User.Identity.GetUserId(),
                        Status = Status.Pendiente,
                        Total = 0.0f
                    };

                    orderManager.Add(pedido);
                }

                //añadimos el producto pedido y su cantidad a la bd
                OrderProduct op = new OrderProduct();
                op.OrderId = pedido.Id;
                op.ProductId = producto.Id;
                op.Number = cantidad;
                opManager.Add(op);

                context.SaveChanges();

            }
            //guardamos las excepciones a la base de datos
            catch(Exception ex)
            {
               
            }
          }

        /// <summary>
        /// Método que abre la ventana del carrito cuando el usuario hace clic sobre Comprar
        /// </summary>
        public void cmdSave_Click(object sender, EventArgs e)
        {
            Response.Redirect("User/Cart");
        }

        /// <summary>
        /// Método que abre la ventana del carrito cuando el usuario hace clic sobre la imagen carrito
        /// </summary>
        public void ImageButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("User/Cart");
        }

        /// <summary>
        /// Metodo que retorna los productos
        /// </summary>
        /// <returns>Lista de productos</returns>
        public List<Product> GetData(string categoria)
        {
            //variables
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager productManager = new ProductManager(context);
            List<Product> list = new List<Product>();

            //lista de productos de cocina
            list = productManager.GetAllByCategory(categoria).ToList();

            return list;
        }



        /// <summary>
        /// Metodo que retorna las imágenes del producto
        /// </summary>
        /// <returns>Lista de imágenes</returns>
        public List<Picture> GetDataPictures(int productId)
        {
            //variables
            ApplicationDbContext context = new ApplicationDbContext();
            PictureManager pictureManager = new PictureManager(context);
            List<Picture> list = new List<Picture>();

            //lista de imágenes del producto
            list = pictureManager.GetAllByProduct(productId).ToList();

            return list;
        }

        protected void ImageButton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect("User/Cart");
        }
    }
}