﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="Muebles.Web.ProductList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:PlaceHolder ID="phAlert" Visible="false" runat="server">
  <div  id="alert" class="alert alert-success fade in alert-dismissible text-center">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   El producto se ha añadido con éxito.</div></asp:PlaceHolder>

    <asp:PlaceHolder ID="ph0" runat="server"><br />
        <asp:ImageButton Style="float: right; margin-bottom: 15px" ID="ImageButton1" OnClick="ImageButton1_Click" runat="server" ImageUrl="~/images/carrito.png" Width="60px" Height="60px" ImageAlign="Right" />
   </asp:PlaceHolder>

       <asp:Label ID="lblVacio" runat="server" CssClass="alert-danger" Text="Lo sentimos, no hay productos de esta categoría." Visible="False"></asp:Label>
       <br />
    <asp:PlaceHolder ID="ph1" runat="server">    </asp:PlaceHolder>

     <br />
    
</asp:Content>
