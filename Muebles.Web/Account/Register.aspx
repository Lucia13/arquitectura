﻿<%@ Page Title="Registro" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Muebles.Web.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2 style="color: #591d1d"><%: Title %></h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Crear una cuenta nueva</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" Width="179px" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="El campo email es obligatorio." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Contraseña</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" TabIndex="1" Width="177px" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="El campo contraseña es obligatorio." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirmar contraseña</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" TabIndex="2" Width="178px" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" ErrorMessage="Tiene que confirmar la contraseña." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" ErrorMessage="Las contraseñas no coinciden." />
            </div>
        </div>
        
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Nombre" CssClass="col-md-2 control-label">Nombre</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Nombre" CssClass="form-control" CausesValidation="True" TabIndex="3" Width="176px"  />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Nombre"
                    CssClass="text-danger" ErrorMessage="El campo nombre es obligatorio."/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Apellidos" CssClass="col-md-2 control-label">Apellidos</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Apellidos" CssClass="form-control" TabIndex="4" Width="179px"  />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Apellidos"
                    CssClass="text-danger" ErrorMessage="El campo apellidos es obligatorio." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Calle" CssClass="col-md-2 control-label">Dirección</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Calle" CssClass="form-control" TabIndex="5" Width="178px" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Calle"
                    CssClass="text-danger" ErrorMessage="El campo dirección es obligatorio." />
            </div>
        </div>


        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Ciudad" CssClass="col-md-2 control-label">Ciudad</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Ciudad" CssClass="form-control" TabIndex="9" Width="179px" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Ciudad"
                    CssClass="text-danger" ErrorMessage="El campo ciudad es obligatorio." />
            </div>
        </div>


        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Provincia" CssClass="col-md-2 control-label">Provincia</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Provincia" CssClass="form-control" CausesValidation="True" TabIndex="10" Width="181px"  />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Provincia"
                    CssClass="text-danger" ErrorMessage="El campo provincia es obligatorio." />
            </div>
        </div>


        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="CP" CssClass="col-md-2 control-label">Código postal</asp:Label>
          <div class="col-md-10">
                <asp:TextBox runat="server" ID="CP" CssClass="form-control" CausesValidation="True" TabIndex="11" Width="183px"  />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="CP"
                    CssClass="text-danger" ErrorMessage="El campo código postal es obligatorio." />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorCP" runat="server" ControlToValidate="CP" CssClass="text-danger" ErrorMessage="Introduzca el código postal en formato correcto." ValidationExpression="^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$"></asp:RegularExpressionValidator>
          </div>
        </div>


        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ddlPais" CssClass="col-md-2 control-label">País</asp:Label>
            <div class="col-md-10" style="display:inline-flex">
               <asp:DropDownList ID="ddlPais" runat="server"  CssClass="form-control" Width="183px" TabIndex="12"></asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="El campo país es obligatorio." />
            </div>
        </div>

         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Telefono" CssClass="col-md-2 control-label">Teléfono</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Telefono" CssClass="form-control" CausesValidation="True" TabIndex="13" Width="183px" TextMode="Phone" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Telefono"
                    CssClass="text-danger" ErrorMessage="El campo teléfono es obligatorio." />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" runat="server" ControlToValidate="Telefono" CssClass="text-danger" ErrorMessage=" Introduzca el teléfono en formato correcto." ValidationExpression="^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$"></asp:RegularExpressionValidator>
            </div>
        </div>

        
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" BorderColor="#591D1D" Font-Bold="True" BackColor="#E6BCCF" ForeColor="#591d1d" OnClick="CreateUser_Click" CssClass="btn btn-default" Text="Registrar" />
            </div>
        </div>

    </div>
</asp:Content>
