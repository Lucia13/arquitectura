﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Muebles.CORE;
using System.Net.Mail;
using Muebles.Application;
using Muebles.DAL;

namespace Muebles.Web.Account
{
    public partial class ForgotPassword : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Metodo que envía enlace para recuperar la contraseña olvidada al email
        /// </summary>
        protected void Forgot(object sender, EventArgs e)
        {
            if (IsValid) //direccion valida
            {

                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                ApplicationUser user = manager.FindByName(Email.Text);

                string code = manager.GeneratePasswordResetToken(user.Id);
                string callbackUrl = IdentityHelper.GetResetPasswordRedirectUrl(code, Request);


                //Creamos un nuevo Objeto de mensaje
                System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

                //Direccion de correo electronico a la que queremos enviar el mensaje
                mmsg.To.Add(new MailAddress(Email.Text));
                //Correo electronico desde la que enviamos el mensaje
                mmsg.From = new System.Net.Mail.MailAddress("luciadaubnerova13@gmail.com");
                //Asunto
                mmsg.Subject = "MiCasa - Restablecer contraseña";
                mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

                //Cuerpo del Mensaje
                mmsg.Body = "Haga clic <a href =\"" + callbackUrl + "\"> aquí</a> para restablecer la contaseña.";
                mmsg.BodyEncoding = System.Text.Encoding.UTF8;
                mmsg.IsBodyHtml = true; //Si no queremos que se envíe como HTML


                /*-------------------------CLIENTE DE CORREO----------------------*/

                //Creamos un objeto de cliente de correo
                System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

                //Hay que crear las credenciales del correo emisor
                cliente.UseDefaultCredentials = false;
                cliente.Credentials = new System.Net.NetworkCredential("luciadaubnerova13@gmail.com", "paulsa14");

                //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail
                cliente.Port = 587;
                cliente.EnableSsl = true;

                //Para Gmail
                cliente.Host = "smtp.gmail.com";


                loginForm.Visible = false;
                DisplayEmail.Visible = true;


                /*-------------------------ENVIO DE CORREO----------------------*/

                try
                {
                    //Enviamos el mensaje      
                    cliente.Send(mmsg);
                    mmsg.Dispose();
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                 
                }



            }
        }
    }
}