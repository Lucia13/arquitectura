﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Muebles.CORE;
using System.Collections.Generic;

namespace Muebles.Web.Account
{
    public partial class Register : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                ddlPais.DataSource = null;
                Dictionary<string, string> valuesEnum = new Dictionary<string, string>();
                var values = Enum.GetValues(typeof(Country));
                foreach (var value in values)
                {
                    valuesEnum.Add(((int)value).ToString(), value.ToString());
                }
                ddlPais.DataTextField = "Value";
                ddlPais.DataValueField = "Key";
                ddlPais.DataSource = valuesEnum;
                ddlPais.DataBind();
            }
        }


        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() {
                UserName = Email.Text,
                Email = Email.Text,
                PhoneNumber = Telefono.Text,
                Name = Nombre.Text,
                Surnames = Apellidos.Text,
                
        };

            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                manager.AddToRole(user.Id, "User");
                signInManager.SignIn( user, isPersistent: false, rememberBrowser: false);
                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            else 
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}