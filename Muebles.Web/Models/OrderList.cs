﻿using Muebles.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Muebles.Web.Models
{
    public class OrderList
    {

        /// <summary>
        /// Id del pedido
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Fecha del pedido
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Nombre del cliente
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>
        public Status Status { get; set; }


        /// <summary>
        /// Total da pagar
        /// </summary>
        public float Total { get; set; }

    }
}