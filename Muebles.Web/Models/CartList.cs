﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Muebles.Web.Models
{
    public class CartList
    {


        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripción del producto
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public float Price { get; set; }

    }
}