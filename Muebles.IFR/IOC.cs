﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using Muebles.Application;
using Muebles.CORE;
using Muebles.DAL;
using Muebles.Web;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Muebles.IFR
{
    /// <summary>
    /// Clase que registra repositorios para realizar inversión de control
    /// </summary>
    public class IOC
    {
        /// <summary>
        /// Método que ejecuta los métodos de la clase IOC
        /// <returns>contenedor de unity</returns>
        /// </summary
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            return container;
        }

        /// <summary>
        /// Método que construye un contenedor de unity
        /// <returns>contenedor de unity</returns>
        /// </summary
        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //registramos repositorios
            container.RegisterType<IProductManager, ProductManager>();
            container.RegisterType<IOrderProductManager, OrderProductManager>();
            container.RegisterType<IOrderManager, OrderManager>();
            container.RegisterType<IPictureManager, PictureManager>();
            container.RegisterType<IAddressManager, AddressManager>();

            container.RegisterType(typeof(UserManager<>), new InjectionConstructor(typeof(IUserStore<>)));
            container.RegisterType<Microsoft.AspNet.Identity.IUser>(new InjectionFactory(c => c.Resolve<Microsoft.AspNet.Identity.IUser>()));
            container.RegisterType(typeof(IUserStore<>), typeof(UserStore<>));
            container.RegisterType<IdentityUser, ApplicationUser>(new ContainerControlledLifetimeManager());
            container.RegisterType<DbContext, ApplicationDbContext>(new ContainerControlledLifetimeManager());

            container.RegisterType<DbContext, ApplicationDbContext>(
    new HierarchicalLifetimeManager());
            container.RegisterType<UserManager<ApplicationUser>>(
                new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(
                new HierarchicalLifetimeManager());

            RegisterTypes(container);
            return container;
        }

        /// <summary>
        /// Método que registra tipos  de repositorio en el contenedor de unity
        /// </summary
        public static void RegisterTypes(IUnityContainer container)
        {

        }
    }
}