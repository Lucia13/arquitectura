﻿using System;
using log4net;

namespace Muebles.IFR
{

    /// <summary>
    /// Clase pra generar logs
    /// </summary>
    public static class Log
    {



        /// <summary>
        /// Método para crear error en el log
        /// </summary>
        /// <param name="message">texto del mensaje</param>
        /// <param name="ex">excepcion producida</param>
        public static void LogError(string message, Exception ex)
        {
            //Escribir en log for net el error
            ILog log = LogManager.GetLogger("EShop");
            log.Error(message, ex);

        }

        /// <summary>
        ///  /// Método para crear alerta en el log
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public static void LogWarn(string message, Exception ex)
        {
            //Escribir en log for net el error
            ILog log = LogManager.GetLogger("EShop");
            log.Warn(message, ex);
        }
    }
}
