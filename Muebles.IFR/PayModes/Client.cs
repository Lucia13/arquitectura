﻿
namespace Muebles.IFR.PayModes
{
    public class Client
    {
        protected IPayMode pago;
        protected string owner;
        protected string number;
        protected int CCV;
        protected string expiration;


        public Client(IPayMode pago, string owner, string number, int CCV, string expiration)
        {
            this.pago = pago;
            this.owner = owner;
            this.number = number;
            this.CCV = CCV;
            this.expiration = expiration;
        }

        public bool Pay(string owner, string number, int CCV, string expiration)
        {
            return this.pago.Pay(this.owner, this.number, this.CCV, this.expiration);
        }
    }
}