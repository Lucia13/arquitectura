﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muebles.IFR.PayModes
{
    public interface IPayMode
    {
        bool Pay(string owner, string number, int CCV, string expiration);
    }
}
